﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineList : MonoBehaviour {
    [SerializeField]
    GameObject machineDetailObject;
    [SerializeField]
    GameObject navBarObject;
    [SerializeField]
    GameObject machineCardObject;
    [SerializeField]
    Transform machineCardParent;
    MachineCard machineCard;
    public delegate void EventOnRender();
    public static EventOnRender OnGameDetail;
    public static EventOnRender OnNavBar;

    private void Awake()
    {
        OnGameDetail += () => {
            machineDetailObject.active = true;
        };
        OnNavBar += () => {
            navBarObject.active = true;
        };
        machineCard = machineCardObject.GetComponent<MachineCard>();
    }
    // Use this for initialization
    private void Start()
    {
        //StartCoroutine(GetGameList());
        //gia su game list trả về game-name
        GenMachineList("May 1", "BattleFish Shooting");

    }
    #region Button
    public void UserInfoClicked()
    {
        IndexController.OnNavBar();
    }
    #endregion
    void GenMachineList(string jBigText, string jSmallText)
    {
        GameObject MachineCardUnit = Instantiate(machineCardObject, machineCardParent);
        MachineCardUnit.GetComponent<MachineCard>().InitMachineCard(jBigText, jSmallText);
    }
}
