﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIO;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class AccountChecking : MonoBehaviour {
    [SerializeField]
    private GameObject InformationObject;
    public Text TotallyProfits;
    public Text LastTimeTotallyProfits;
    public Text ThisTimeNetProfits;
    public Text AddingScoresNumber;
    public Text ReducingScoreNumber;
    public Text InseringCoinQuanity;
    public Text ReturnCoinQuantity;
    public Text Ticket;
    public Text RemainingRunTime;
    public Text EnteringCodeTime;
    Datalogger datalogger;
	// Use this for initialization
	void Start () {
        datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();
        string _socketid = datalogger.SocketID;
        StartCoroutine(EmitAccountCheck(_socketid));
        SKIO.socket.On("GETAC", AccountCheck);
	}
    IEnumerator EmitAccountCheck(string _socketid)
    {
        yield return new WaitForSeconds(0.1f);
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["socketid"] = _socketid;
        if (SKIO.socket != null)
        {
            SKIO.socket.Emit("ACCOUNTCHECKING", new JSONObject(data));
        }


    }
    //Khiêm : Nhận data từ db game->server->app
    //On ACCOUNTCHECKING
    public void AccountCheck(SocketIOEvent msg)
    {
        //set dữ liệu cho các Text
        TotallyProfits.text = msg.data["TotallyProfits"].ToString().Split('\"')[1];
        LastTimeTotallyProfits.text = msg.data["LastTimeTotallyProfits"].ToString().Split('\"')[1];
        ThisTimeNetProfits.text = msg.data["ThisTimeNetProfits"].ToString().Split('\"')[1];
        AddingScoresNumber.text = msg.data["AddingScoresNumber"].ToString().Split('\"')[1];
        ReducingScoreNumber.text = msg.data["ReducingScoreNumber"].ToString().Split('\"')[1];
        InseringCoinQuanity.text = msg.data["InseringCoinQuanity"].ToString().Split('\"')[1];
        ReturnCoinQuantity.text = msg.data["ReturnCoinQuantity"].ToString().Split('\"')[1];
        Ticket.text = msg.data["Ticket"].ToString().Split('\"')[1];
        RemainingRunTime.text = msg.data["RemainingRunTime"].ToString().Split('\"')[1];
        EnteringCodeTime.text = msg.data["EnteringCodeTime"].ToString().Split('\"')[1];
    }
    public void BackButtonClicked()
    {
        SceneManager.LoadScene("RunReports");
    }
    public void InfoButtonClick()
    {
        InformationObject.active = !InformationObject.active;
    }
}
