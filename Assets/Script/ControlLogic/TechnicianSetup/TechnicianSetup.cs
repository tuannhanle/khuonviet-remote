﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TechnicianSetup : MonoBehaviour {

    public GameObject prefabLogout;
    Datalogger datalogger;
    public Text VersionNumber;
    public Text GameBoxId;
	// Use this for initialization
	void Start () {
        datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();
        GameBoxId.text = datalogger.GameboxID;
        VersionNumber.text = datalogger.MachineVersion;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetDefaultButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();
        SceneManager.LoadScene("ParameterSetting");
    }
    public void SetwifiButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();
        SceneManager.LoadScene("ExcuteCommand");//Wifi
    }
    public void CustomerButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();
        SceneManager.LoadScene("CustomerSetup");
    }
    public void CodeSettingClicked()
    {
        Sound.Singleton.PlaySoundButton();
        SceneManager.LoadScene("CodeSetting");

    }
    public void ChangePasswordButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();
        SceneManager.LoadScene("ChangePassword");
    }
    public void SetPercentButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();
        SceneManager.LoadScene("SetPercent");

    }
    public void LogoutButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();
        prefabLogout.SetActive(true);

    }
    public void BackButtonClicked()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
