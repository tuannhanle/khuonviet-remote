﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    private Text versionNumber;
    [SerializeField]
    private Text machineID;
    // Use this for initialization
    void Start()
    {
        Datalogger datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();
        versionNumber.text = datalogger.GameboxID.Substring(0, 3);
        machineID.text = string.Format("{0} {1} {2} {3} {4}",
            datalogger.GameboxID.Substring(0, 3),
            datalogger.GameboxID.Substring(3, 2),
            datalogger.GameboxID.Substring(5, 1),
            datalogger.GameboxID.Substring(6, 4),
            datalogger.GameboxID.Substring(10)
            );


    }

    // Update is called once per frame
    void Update()
    {

    }

    public void WarehouseSetupButotn()
    {
        SceneManager.LoadScene("WarehouseSetup");

    }
    public void TechnicianSetupButotn()
    {
        SceneManager.LoadScene("TechnicianSetup");

    }
    public void OwnerSetupButotn()
    {
        SceneManager.LoadScene("OwnerSetup");

    }
    public void OperaterSetupButotn()
    {
        SceneManager.LoadScene("OperatorSetup");

    }
    public void ScanButton()
    {
        Destroy(GameObject.FindGameObjectWithTag("datalogger"));
        Destroy(GameObject.FindGameObjectWithTag("socketioindex"));
        Destroy(GameObject.FindGameObjectWithTag("sound"));

        SKIO.socket.Close();
        SceneManager.LoadScene("Gateway");

    }
}
