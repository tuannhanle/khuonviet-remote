﻿using SocketIO;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;


public class ParameterSetting : MonoBehaviour
{

    public GameObject prefabLogout;
    [SerializeField]
    private GameObject InformationObject;
    [SerializeField]
    private ToggleGroup GameDifficultToggleGroup;
    [SerializeField]
    private Toggle[] GameDifficultToggles;
    [SerializeField]
    private ToggleGroup LanguageToggleGroup;
    [SerializeField]
    private Toggle[] LanguageToggles;
    [SerializeField]
    private Button[] GunLayoutType_LeftRightButtons;
    [SerializeField]
    private Text GunLayoutTypeField;
    [SerializeField]
    private Button[] TicketRationCoin_LeftRightButtons;
    [SerializeField]
    private Text TicketRationCoinField;
    [SerializeField]
    private Button[] TicketRationTicket_LeftRightButtons;
    [SerializeField]
    private Text TicketRationTicketField;
    [SerializeField]
    private Toggle IsBulletCrossWhenScreenNetToggle;
    [SerializeField]
    private InputField ScoreChangeValueField;
    [SerializeField]
    private InputField ScoreMaxField;
    [SerializeField]
    private InputField ScoreMinField;
    [SerializeField]
    private InputField NumScoreUpMaxField;
    [SerializeField]
    private Text Notification;

    public void LogoutButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        prefabLogout.SetActive(true);

    }
    public enum GameDifficult
    {
        VeryEasy,
        Easy,
        Hard,
        VeryHard,
        DieHard
    }
    public enum GunLayoutType
    {
        //W10,W8,W6,W4,L4,L3,
        L1, L2, L3, L4, W4, W6, W8, W10,                                                        //单屏
        S_L6S, S_L6D, S_L8S, S_L8D, S_W8S, S_W8D, S_W12S, S_W16S, S_W20S,                //双屏 Màn hình kép
        L_L2S, L_L2D, L_L4S, L_L4D, L_L6S, L_L6D, L_L8S, L_L8D, L_W8S, L_W8D, L_W10S, L_W10D, L_W12S, L_W14S, L_W16S//联屏 Màn hình chung
    }
    public enum Language
    {
        Vn,
        En
    }
    GameDifficult GameDifficult_;
    GunLayoutType GunLayoutType_;
    Language LanguageUsing;
    int CoinTicketRatio_Coin;
    int CoinTicketRatio_Ticket;
    bool IsBulletCrossWhenScreenNet;

    string ScoreChangeValue;
    string ScoreMax;
    string ScoreMin;
    string NumScoreUpMax;

    Datalogger datalogger;


    Dictionary<string, string> parameter = new Dictionary<string, string>();
    public void BackButtonClick()
    {
        StartCoroutine(EmitBackParameterSetting(parameter));

    }
    public void InfoButtonClick()
    {
        InformationObject.active= !InformationObject.active;
    }
    private void Awake()
    {
        datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();

    }
    private void Start()
    {
        parameter["socketid"] = datalogger.SocketID;
        StartCoroutine(EmitEnParameterSetting(parameter));

        SKIO.socket.On("DEPARAMETERSETTING", (SocketIOEvent e) =>
        {
            Debug.Log(string.Format("DEPARAMETERSETTING:[name: {0}, data: {1}]", e.name, e.data));
            Debug.Log(e.data["GameDifficult_"].ToString().Trim('\"'));

            GameDifficult_ = (GameDifficult)System.Enum.GetValues(typeof(GameDifficult)).GetValue(Int32.Parse(e.data["GameDifficult_"].ToString().Trim('\"')));
            LanguageUsing = (Language)System.Enum.GetValues(typeof(Language)).GetValue(Int32.Parse(e.data["LaguageUsing"].ToString().Trim('\"')));
            GunLayoutType_ = (GunLayoutType)System.Enum.GetValues(typeof(GunLayoutType)).GetValue(Int32.Parse(e.data["GunLayoutType_"].ToString().Trim('\"')));
            CoinTicketRatio_Coin = Int32.Parse(e.data["CoinTicketRatio_Coin"].ToString().Trim('\"'));
            CoinTicketRatio_Ticket = Int32.Parse(e.data["CoinTicketRatio_Ticket"].ToString().Trim('\"')); 
            IsBulletCrossWhenScreenNet = Convert.ToBoolean(e.data["IsBulletCrossWhenScreenNet"].ToString().Trim('\"'));
            ScoreChangeValue = e.data["ScoreChangeValue"].ToString().Trim('\"'); 
            ScoreMax = e.data["ScoreMax"].ToString().Trim('\"'); 
            ScoreMin = e.data["ScoreMin"].ToString().Trim('\"'); 
            NumScoreUpMax = e.data["NumScoreUpMax"].ToString().Trim('\"');

            GameDifficultToggles[(int)GameDifficult_].isOn = true;
            LanguageToggles[(int)LanguageUsing].isOn = true;
            GunLayoutTypeField.text = GunLayoutType_.ToString();
            GunLayoutTypeSetting();
            TicketRationCoinSetting();
            TicketRationTicketSetting();
            ScoreChangeValueField.text = ScoreChangeValue.ToString();
            ScoreMaxField.text = ScoreMax.ToString();
            ScoreMinField.text = ScoreMin.ToString();
            NumScoreUpMaxField.text = NumScoreUpMax.ToString();
            IsBulletCrossWhenScreenNetToggle.isOn = IsBulletCrossWhenScreenNet;


        });
        SKIO.socket.On("PARAMETERSETTINGDONE", (SocketIOEvent e) =>
        {
            Debug.Log(string.Format("PARAMETERSETTINGDONE:[name: {0}, data: {1}]", e.name, e.data));
            this.Notification.text = "Set Parameter Successfully";
        });
        //InitDefaultUI();

        //test
        //DefaultSettingClicked();
    }
    //private void InitDefaultUI()
    //{
    //    SetDefaultParameter();
    //    GameDifficultToggles[(int)GameDifficult_].isOn = true;
    //    LanguageToggles[(int)LanguageUsing].isOn = true;
    //    GunLayoutTypeField.text = GunLayoutType_.ToString();
    //    GunLayoutTypeSetting();
    //    TicketRationCoinSetting();
    //    TicketRationTicketSetting();
    //    ScoreChangeValueField.text = ScoreChangeValue.ToString();
    //    ScoreMaxField.text = ScoreMax.ToString();
    //    ScoreMinField.text = ScoreMin.ToString();
    //    NumScoreUpMaxField.text = NumScoreUpMax.ToString();
    //    IsBulletCrossWhenScreenNetToggle.isOn = IsBulletCrossWhenScreenNet;
    //}
    public void DefaultSettingClicked()
    {
        SetDefaultParameter();
        GameDifficultToggles[(int)GameDifficult_].isOn = true;
        LanguageToggles[(int)LanguageUsing].isOn = true;
        GunLayoutTypeField.text = GunLayoutType_.ToString();
        TicketRationCoinField.text = CoinTicketRatio_Coin.ToString();
        TicketRationTicketField.text = CoinTicketRatio_Ticket.ToString();
        ScoreChangeValueField.text = ScoreChangeValue.ToString();
        ScoreMaxField.text = ScoreMax.ToString();
        ScoreMinField.text = ScoreMin.ToString();
        NumScoreUpMaxField.text = NumScoreUpMax.ToString();
        IsBulletCrossWhenScreenNetToggle.isOn = IsBulletCrossWhenScreenNet;

    }
    public void CustomSettingClicked()
    {
        GameDifficultSetting();
        LanguageSetting();
        BulletCrossWhenScreenNetToggleSetting();
        ScoreChangeValueSetting();
        ScoreMaxSetting();
        ScoreMinSetting();
        NumScoreUpMaxSetting();
        ConvertParameterToJSON();
        StartCoroutine(EmitParameterSetting(parameter));
    }
    void SetDefaultParameter()
    {
        GameDifficult_ = GameDifficult.Easy;
        CoinTicketRatio_Coin = 1;
        CoinTicketRatio_Ticket = 1;
        GunLayoutType_ = GunLayoutType.W8;
        IsBulletCrossWhenScreenNet = false;
        ScoreChangeValue = "100";
        ScoreMax = "9900";
        ScoreMin = "10";
        LanguageUsing = Language.En;
        NumScoreUpMax = "5000000";

    }
    void ConvertParameterToJSON()
    {
        parameter["GameDifficult_"] = ((int)GameDifficult_).ToString();
        parameter["CoinTicketRatio_Coin"] = CoinTicketRatio_Coin.ToString();
        parameter["CoinTicketRatio_Ticket"] = CoinTicketRatio_Ticket.ToString();
        parameter["GunLayoutType_"] = ((int)GunLayoutType_).ToString();
        parameter["IsBulletCrossWhenScreenNet"] = IsBulletCrossWhenScreenNet.ToString();
        parameter["ScoreChangeValue"] = ScoreChangeValue.ToString();
        parameter["ScoreMax"] = ScoreMax.ToString();
        parameter["ScoreMin"] = ScoreMin.ToString();
        parameter["LaguageUsing"] = ((int)LanguageUsing).ToString();
        parameter["NumScoreUpMax"] = NumScoreUpMax.ToString();
        parameter["socketid"] = datalogger.SocketID;


    }
    IEnumerator EmitEnParameterSetting(Dictionary<string, string> _parameter)
    {
        yield return new WaitForSeconds(0.1f);
        if (SKIO.socket != null)
        {
            SKIO.socket.Emit("ENPARAMETERSETTING", new JSONObject(_parameter));
        }

    }
    IEnumerator EmitParameterSetting(Dictionary<string, string> _parameter)
    {
        yield return new WaitForSeconds(0.1f);
        if (SKIO.socket != null)
        {
            SKIO.socket.Emit("PARAMETERSETTING", new JSONObject(_parameter));
        }

    }
    IEnumerator EmitBackParameterSetting(Dictionary<string, string> _parameter)
    {
        yield return new WaitForSeconds(0.1f);
        if (SKIO.socket != null)
        {
            SKIO.socket.Emit("BACKPARAMETERSETTING", new JSONObject(_parameter));
            SceneManager.LoadScene("OwnerSetup");

        }

    }
    void GameDifficultSetting()
    {

        //GameDifficultToggles[(int)GameDifficult_].isOn = true;
        for (int i = 0; i < System.Enum.GetValues(typeof(GameDifficult)).Length; i++)
        {
            if (GameDifficultToggles[i].isOn == true)
            {
                GameDifficult_ = (GameDifficult)System.Enum.GetValues(typeof(GameDifficult)).GetValue(i);
            }
        }

    }
    void LanguageSetting()
    {
        //LanguageToggles[(int)LanguageUsing].isOn = true;
        for (int i = 0; i < System.Enum.GetValues(typeof(Language)).Length; i++)
        {
            if (LanguageToggles[i].isOn == true)
            {
                LanguageUsing = (Language)System.Enum.GetValues(typeof(Language)).GetValue(i);
            }
        }
    }
    void GunLayoutTypeSetting()
    {
        GunLayoutType_LeftRightButtons[0].onClick.AddListener(() =>
        {
            if ((int)GunLayoutType_ > 0)
            {
                GunLayoutType_--;
                GunLayoutTypeField.text = GunLayoutType_.ToString();
            }


        });
        GunLayoutType_LeftRightButtons[1].onClick.AddListener(() =>
        {
            if ((int)GunLayoutType_ < System.Enum.GetValues(typeof(GunLayoutType)).Length - 1)
            {
                GunLayoutType_++;
                GunLayoutTypeField.text = GunLayoutType_.ToString();
            }

        });
    }
    void TicketRationCoinSetting()
    {
        TicketRationCoinField.text = CoinTicketRatio_Coin.ToString();
        TicketRationCoin_LeftRightButtons[0].onClick.AddListener(() =>
        {
            if (CoinTicketRatio_Coin > 0)
            {
                CoinTicketRatio_Coin--;
                TicketRationCoinField.text = CoinTicketRatio_Coin.ToString();
            }


        });
        TicketRationCoin_LeftRightButtons[1].onClick.AddListener(() =>
        {

            CoinTicketRatio_Coin++;
            TicketRationCoinField.text = CoinTicketRatio_Coin.ToString();


        });
    }
    void TicketRationTicketSetting()
    {
        TicketRationTicketField.text = CoinTicketRatio_Ticket.ToString();
        TicketRationTicket_LeftRightButtons[0].onClick.AddListener(() =>
        {
            if (CoinTicketRatio_Ticket > 0)
            {
                CoinTicketRatio_Ticket--;
                TicketRationTicketField.text = CoinTicketRatio_Ticket.ToString();
            }


        });
        TicketRationTicket_LeftRightButtons[1].onClick.AddListener(() =>
        {

            CoinTicketRatio_Ticket++;
            TicketRationTicketField.text = CoinTicketRatio_Ticket.ToString();


        });
    }
    void ScoreChangeValueSetting()
    {
        //ScoreChangeValueField.text = ScoreChangeValue.ToString();
        ScoreChangeValue = ScoreChangeValueField.text;
    }
    void ScoreMaxSetting()
    {
        //ScoreMaxField.text = ScoreMax.ToString();
        ScoreMax = ScoreMaxField.text;
    }
    void ScoreMinSetting()
    {
        //ScoreMinField.text = ScoreMin.ToString();
        ScoreMin = ScoreMinField.text;
    }
    void NumScoreUpMaxSetting()
    {
        //NumScoreUpMaxField.text = NumScoreUpMax.ToString();
        NumScoreUpMax = NumScoreUpMaxField.text;
    }
    void BulletCrossWhenScreenNetToggleSetting()
    {
        IsBulletCrossWhenScreenNet = IsBulletCrossWhenScreenNetToggle.isOn;
    }
}
