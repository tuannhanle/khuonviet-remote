﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using SocketIO;
public class Gateway : MonoBehaviour
{
    [SerializeField]
    private string urlLocal, urlGlobal;
    [SerializeField]
    private SocketIOComponent _socketIOComponent;


    private void Start()
    {
        GeneratorGamebox.FindByPhone = "";//Reset phone to find all gamebox

    }
    private void Awake()
    {

        
    }

    public void LocalButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();
        _socketIOComponent.url = urlLocal;
        LoadScene();

    }
    public void GlobalButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();
        _socketIOComponent.url = urlGlobal;
        LoadScene();
    }
    void LoadScene()
    {
        _socketIOComponent.Close();
        _socketIOComponent.InitAwake();
        _socketIOComponent.Connect();

        SKIO.trigger = true;
        SceneManager.LoadScene("ChoosingMachine");

    }
    
}
