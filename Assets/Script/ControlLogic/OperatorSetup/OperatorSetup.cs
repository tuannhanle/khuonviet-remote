﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OperatorSetup : MonoBehaviour {

    public GameObject prefabLogout;
    Datalogger datalogger;
	// Use this for initialization
	void Start () {
        datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void PhonenumberButtonClicked()
    {
        //SceneManager.LoadScene("");
    }
    public void MachineIDButtonClicked()
    {
        //SceneManager.LoadScene("");
    }

    public void OperatorPasswordButtonClicked()
    {
        SceneManager.LoadScene("ChangePassword");
    }
    public void LogoutButtonClicked()
    {
        prefabLogout.SetActive(true);
        
    }
    public void BackButtonClicked()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
