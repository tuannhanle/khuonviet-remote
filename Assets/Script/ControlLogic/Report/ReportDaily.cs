﻿using SocketIO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class ReportDaily : MonoBehaviour {
    [SerializeField]
    private Text _profit, _gainAmount, _dragonWallet, _totalIn, _totalOut, _percentHolding, _maxProfit, _totalPlayerCredit, createAt,notification;
    public GameObject CalenderPannel;
    Datalogger datalogger;
    Dictionary<string, string> parameter = new Dictionary<string, string>();
    private float _timecounter = 0;

    private void Awake()
    {
        try
        {
            datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();
            parameter["socketid"] = datalogger.SocketID;
        }
        catch (System.Exception)
        {


        }

    }
    void Start()
    {
        //CalenderPannel.SetActive(true);
        //StartCoroutine(EmitReport());
        SKIO.socket.On("Khiem_GET_MONTHLYREPORT", (SocketIOEvent e) =>
        {
            Debug.Log(string.Format("DAILYREPORT:[name: {0}, data: {1}]", e.name, e.data));
            UpdateUI(e);
        });
        SKIO.socket.On("Khiem_SEND_DAILYREPORT", (SocketIOEvent e) =>
        {
            Debug.Log(string.Format("Khiem_SEND_DAILYREPORT:[name: {0}, data: {1}]", e.name, e.data));

            if(e.data["success"].ToString()=="true"){
                Debug.Log("true1");       
                UpdateUI(e);
            }
            else
            {
   
                Debug.Log("false1");
                UpdateDefaultUI();
            }
           
        });
    }
    void UpdateDefaultUI()
    {
        _profit.text = "N/A";
        _gainAmount.text = "N/A";
        _dragonWallet.text = "N/A";
        _totalIn.text = "N/A";
        _totalOut.text = "N/A";
        _percentHolding.text = "N/A";
        _maxProfit.text = "N/A";
        _totalPlayerCredit.text = "N/A";
        notification.text = "Not Found Day in DB";
    }
    void UpdateUI(SocketIOEvent e)
    {
        _profit.text = (
              float.Parse(e.data["Dat_Profit"].ToString().Trim('\"'))
            + float.Parse(e.data["Dat_GainAmount"].ToString().Trim('\"'))
            + float.Parse(e.data["Dat_DragonWallet"].ToString().Trim('\"'))
            + float.Parse(e.data["Dat_BonusWallet"].ToString().Trim('\"'))
        ) + " point";
        //_profit.text = e.data["Dat_Profit"].ToString().Trim('\"') + " point";
        //_gainAmount.text = e.data["Dat_GainAmount"].ToString().Trim('\"') + " point";
        //_dragonWallet.text = e.data["Dat_DragonWallet"].ToString().Trim('\"') + " point";
        _totalIn.text = e.data["Dat_TotalIn"].ToString().Trim('\"') + " point";
        _totalOut.text = e.data["Dat_TotalOut"].ToString().Trim('\"') + " point";
        _percentHolding.text = e.data["Dat_Percent"].ToString().Trim('\"') + " %";
        _maxProfit.text = e.data["Dat_MaxProfit"].ToString().Trim('\"') + " point";
        _totalPlayerCredit.text = e.data["Dat_TotalPlayerCredit"].ToString().Trim('\"') + " point";
        notification.text = e.data["createAt"].ToString().Trim('\"');
    }
    
    public void BackButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        SceneManager.LoadScene("RunReports");
    }
    public void InputDayButtonClicked() {
        CalenderPannel.SetActive(true);
    }
    public void OKButtonClicked()
    {
        StartCoroutine(EmitReport());//in socket.On(getdatareport)
        
        //StartCoroutine(EmitReport());
    }
    private IEnumerator GetReportData()
    {
        Dictionary<string, string> passport = new Dictionary<string, string>();
        Debug.Log(createAt.text);
        string[] temp = createAt.text.Split('/');

        passport["gameboxID"] = datalogger.GameboxID;
        passport["createAt"] = temp[0] + '/' + temp[1] + '/' + temp[2].Split(' ')[0];
        Debug.Log(temp[0] + '/' + temp[1] + '/' + temp[2].Split(' ')[0]);


        // Delete cookie before requesting a new login
        webservices.CookieString = null;

        var request = webservices.Post("Get_dailyreport", new JSONObject(passport).ToString());
        yield return request.Send();

        if (request.isNetworkError)
        {
            Debug.LogError(request.error);
        }
        else
        {
            webservices.CookieString = request.GetResponseHeader("set-cookie");
            //Debug.Log(webservices.CookieString);
            Debug.Log(request.downloadHandler.text);
        }
    }
    IEnumerator EmitReport()
    {
        yield return new WaitForSeconds(0.1f);

        //parameter["createAt"] = "6/7/2019";
        string[] temp = createAt.text.Split('/');
        parameter["gameboxID"] = datalogger.GameboxID;
        parameter["createAt"] = temp[0] + '/' + temp[1] + '/' + temp[2].Split(' ')[0];
        parameter["socketid"] = datalogger.SocketID;


        Debug.Log(temp[0] + '/' + temp[1] + '/' + temp[2].Split(' ')[0]);

        if (SKIO.socket != null)
        {
            SKIO.socket.Emit("Khiem_GET_DAILYREPORT", new JSONObject(parameter));

        }

    }
}
