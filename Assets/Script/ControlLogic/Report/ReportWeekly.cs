﻿using SocketIO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class ReportWeekly : MonoBehaviour {
    [SerializeField]
    private Text _profit, _gainAmount, _dragonWallet, _totalIn, _totalOut,
        _percentHolding, _maxProfit, _totalPlayerCredit, createAt, notification;
    public GameObject CalenderPannel;
    Datalogger datalogger;
    Dictionary<string, string> parameter = new Dictionary<string, string>();
    private float _timecounter = 0;
    JSONObject data;
    private void Awake()
    {
        try
        {
            datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();
            parameter["socketid"] = datalogger.SocketID;
        }
        catch (System.Exception)
        {


        }

    }
    void Start()
    {
        //CalenderPannel.SetActive(true);
        //StartCoroutine(EmitReport());
        SKIO.socket.On("Khiem_GET_WEEKLYREPORT", (SocketIOEvent e) =>
        {
            Debug.Log(string.Format("Khiem_GET_WEEKLYREPORT:[name: {0}, data: {1}]", e.name, e.data));
            UpdateUI(e);
        });
        SKIO.socket.On("Khiem_SEND_WEEKLYREPORT", (SocketIOEvent e) =>
        {
            Debug.Log(string.Format("Khiem_SEND_WEEKLYREPORT:[name: {0}, data: {1}]", e.name, e.data));
            if (e.data["success"].ToString() == "true")
            {
                Debug.Log("true1");
                UpdateUI(e.data["data"]);
            }
            else
            {
                notification.text = "Not Found Day in DB";
                Debug.Log("false1");
                UpdateDefaultUI();
            }
        });
    }
    IEnumerator EmitReport( )
    {
        yield return new WaitForSeconds(0.1f);
        


        parameter["gameboxID"] = datalogger.GameboxID;
        parameter["socketid"] = datalogger.SocketID;
        //parameter["createAt"] = "6/7/2019";
        string[] temp = createAt.text.Split('/');
        parameter["createAt"] = temp[0] + '/' + temp[1] + '/' + temp[2].Split(' ')[0];
        Debug.Log(temp[0] + '/' + temp[1] + '/' + temp[2].Split(' ')[0]);

        if (SKIO.socket != null )
        {
            SKIO.socket.Emit("Khiem_GET_WEEKLYREPORT", new JSONObject(parameter));

        }

    }
    void UpdateUI(JSONObject data)
    {
        float _profit = 0, _gainAmount = 0, _bonusWallet = 0, _dragonWallet = 0, _totalIn = 0, _totalOut = 0, _percentHolding, _maxProfit = 0, _totalPlayerCredit = 0;
        string listDayOfWeek="";
        for (int i = 0; i < data.Count; i++)
        {
            _profit += float.Parse(data[i]["Dat_Profit"].ToString().Trim('\"'));
            _gainAmount += float.Parse(data[i]["Dat_GainAmount"].ToString().Trim('\"'));
            _bonusWallet += float.Parse(data[i]["Dat_BonusWallet"].ToString().Trim('\"'));
            _dragonWallet += float.Parse(data[i]["Dat_DragonWallet"].ToString().Trim('\"'));
            _totalIn += float.Parse(data[i]["Dat_TotalIn"].ToString().Trim('\"'));
            _totalOut += float.Parse(data[i]["Dat_TotalOut"].ToString().Trim('\"'));
            //_percentHolding += float.Parse(data[i]["Dat_Percent"].ToString().Trim('\"'));
            _maxProfit += float.Parse(data[i]["Dat_MaxProfit"].ToString().Trim('\"'));
            _totalPlayerCredit += float.Parse(data[i]["Dat_TotalPlayerCredit"].ToString().Trim('\"'));

            ////danh sach ngay trong tuan
            listDayOfWeek += data[i]["createAt"].ToString().Trim('\"') + ',';
        

        }
        Debug.Log(listDayOfWeek);
        this._profit.text = (_profit + _gainAmount+ _dragonWallet + _bonusWallet) + " point";
        //this._gainAmount.text = _gainAmount.ToString() + " point";
        //this._dragonWallet.text = _dragonWallet.ToString() + " point";
        this._totalIn.text = _totalIn.ToString() + " point";
        this._totalOut.text = _totalOut.ToString() + " point";
        this._percentHolding.text = data[0]["Dat_Percent"].ToString().Trim('\"') + " %";
        this._maxProfit.text = _maxProfit.ToString() + " point";
        this._totalPlayerCredit.text = _totalPlayerCredit.ToString() + " point";

        this.notification.text = listDayOfWeek.TrimEnd(',');
    }
    void UpdateDefaultUI()
    {
        _profit.text = "N/A";
        _gainAmount.text = "N/A";
        _dragonWallet.text = "N/A";
        _totalIn.text = "N/A";
        _totalOut.text = "N/A";
        _percentHolding.text = "N/A";
        _maxProfit.text = "N/A";
        _totalPlayerCredit.text = "N/A";

    }
    void UpdateUI(SocketIOEvent e)
    {
        _profit.text = e.data["Dat_Profit"].ToString().Trim('\"') + " point";
        _gainAmount.text = e.data["Dat_GainAmount"].ToString().Trim('\"') + " point";
        _dragonWallet.text = e.data["Dat_DragonWallet"].ToString().Trim('\"') + " point";
        _totalIn.text = e.data["Dat_TotalIn"].ToString().Trim('\"') + " point";
        _totalOut.text = e.data["Dat_TotalOut"].ToString().Trim('\"') + " point";
        _percentHolding.text = e.data["Dat_Percent"].ToString().Trim('\"') + " %";
        _maxProfit.text = e.data["Dat_MaxProfit"].ToString().Trim('\"') + " point";
        _totalPlayerCredit.text = e.data["Dat_TotalPlayerCredit"].ToString().Trim('\"') + " point";
    }
    public void BackButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        SceneManager.LoadScene("RunReports");
    }
    public void InputDayButtonClicked() {
        CalenderPannel.SetActive(true);
    }
    public void OKButtonClicked()
    {
        
        StartCoroutine(EmitReport());
    }
}
