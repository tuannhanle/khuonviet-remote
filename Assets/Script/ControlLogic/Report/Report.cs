﻿using SocketIO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Report : MonoBehaviour {
    [SerializeField]
    private Text _profit, _gainAmount, _dragonWallet,_bonusWallet, _totalIn, _totalOut, _percentHolding, _maxProfit, _totalPlayerCredit;
    Datalogger datalogger;
    Dictionary<string, string> parameter = new Dictionary<string, string>();
    private float _timecounter = 0;

    private void Awake()
    {
        try
        {
            datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();
            parameter["socketid"] = datalogger.SocketID;
        }
        catch (System.Exception)
        {


        }

    }
    void Start()
    {
        StartCoroutine(EmitReport());

        SKIO.socket.On("DAILYREPORT", (SocketIOEvent e) =>
        {
            Debug.Log(string.Format("DAILYREPORT:[name: {0}, data: {1}]", e.name, e.data));
            UpdateUI(e);
        });
        SKIO.socket.On("WEEKLYREPORT", (SocketIOEvent e) =>
        {
            Debug.Log(string.Format("WEEKLYREPORT:[name: {0}, data: {1}]", e.name, e.data));
            UpdateUI(e);
        });
        SKIO.socket.On("MONTHLYREPORT", (SocketIOEvent e) =>
        {
            Debug.Log(string.Format("MONTHLYREPORT:[name: {0}, data: {1}]", e.name, e.data));
            UpdateUI(e);
        });

    }
    private void Update()
    {
        _timecounter = _timecounter + Time.deltaTime;
        // each 2 seconds per 1 time
        if (_timecounter > 10 )
        {
            //Debug.Log("Per 2s");
            _timecounter = 0;
            StartCoroutine(EmitReport());
        }
    }
    IEnumerator EmitReport( )
    {
        yield return new WaitForSeconds(0.1f);

        if (SKIO.socket != null )
        {
            SKIO.socket.Emit("ENREPORT", new JSONObject(parameter));

        }

    }
    void UpdateUI(SocketIOEvent e)
    {
        _profit.text = (
              float.Parse(e.data["Dat_Profit"].ToString().Trim('\"'))
            + float.Parse(e.data["Dat_GainAmount"].ToString().Trim('\"'))
            + float.Parse(e.data["Dat_DragonWallet"].ToString().Trim('\"'))
            + float.Parse(e.data["Dat_BonusWallet"].ToString().Trim('\"'))
        ) + " point";
        //_profit.text = e.data["Dat_Profit"].ToString().Trim('\"') + " point";
        //_gainAmount.text = e.data["Dat_GainAmount"].ToString().Trim('\"') + " point";
        //_dragonWallet.text = e.data["Dat_DragonWallet"].ToString().Trim('\"') + " point";
        //_bonusWallet.text = e.data["Dat_BonusWallet"].ToString().Trim('\"') + " point";
        _totalIn.text = e.data["Dat_TotalIn"].ToString().Trim('\"') + " point";
        _totalOut.text = e.data["Dat_TotalOut"].ToString().Trim('\"') + " point";
        _percentHolding.text = e.data["Dat_Percent"].ToString().Trim('\"') + " %";
        _maxProfit.text = e.data["Dat_MaxProfit"].ToString().Trim('\"') + " point";
        _totalPlayerCredit.text = e.data["Dat_TotalPlayerCredit"].ToString().Trim('\"') + " point";
    }
    public void BackButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        SceneManager.LoadScene("RunReports");
    }
}
