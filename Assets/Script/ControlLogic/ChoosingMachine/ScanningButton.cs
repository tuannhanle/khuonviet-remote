﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScanningButton : MonoBehaviour {
    [SerializeField]
    GameObject _scanButton;
    [SerializeField]
    GameObject _scanningText;

    public void ScanButtonClicked()
    {
        _scanButton.SetActive(false);

        if (ScanningProcess())
        {
            this.gameObject.SetActive(false);
        }

    }

    private bool ScanningProcess()
    {
        _scanningText.SetActive(true);

        bool done = false;
        done = true;
        if (done)
        {
            return true;

        }
        else
        {
            _scanningText.SetActive(false);
            _scanButton.SetActive(true  );

            return false;
        }
    }
}
