﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using SocketIO;
public class LogOutGame : MonoBehaviour {

	// Use this for initialization
    public string _socketID;
    Datalogger datalogger;

    [SerializeField]
    GameObject GO_SKIO;
    void Awake()
    {
        datalogger = this.gameObject.GetComponent<Datalogger>();

    }
    void Start () {
        SKIO.socket.On("ROOMDROP", (SocketIOEvent e) =>
        {
            
            string roomID = e.data["rooms"][0].ToString().Trim('\"');
            _socketID = datalogger.SocketID;
            Debug.Log(roomID);
            if (roomID == _socketID)
            {
                Debug.Log(roomID);
                Destroy(GameObject.FindGameObjectWithTag("socketioindex"));
                Destroy(GameObject.FindGameObjectWithTag("datalogger"));
                Destroy(GameObject.FindGameObjectWithTag("sound"));
                GeneratorGamebox.ListRoomGame.Remove(_socketID);
                SKIO.socket.Close();
                SceneManager.LoadScene("Gateway");
            }
        });
	}

	// Update is called once per frame
	void Update () {
        
	}
}
