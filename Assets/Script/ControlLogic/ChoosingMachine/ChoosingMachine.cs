﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChoosingMachine : MonoBehaviour {

    [SerializeField]
    GameObject FindbyPhone;
	// Use this for initialization
	void Start () {
		
	}
    public void ShowFindbyPhoneClicked()
    {
        FindbyPhone.SetActive(true);
    }
    public void BackButtonClicked()
    {
        Destroy(GameObject.FindGameObjectWithTag("socketioindex"));
        Destroy(GameObject.FindGameObjectWithTag("datalogger"));
        Destroy(GameObject.FindGameObjectWithTag("sound"));

        SKIO.socket.Close();
        SceneManager.LoadScene("Gateway");

    }
    // Update is called once per frame
    void Update () {
		
	}
}
