﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using SocketIO;

public class GameboxPicked : MonoBehaviour {
    Image Bg;
    Text gameBoxText;
    public Color InitializedColor;
    public Color yetInitializedColor;
    [SerializeField]
    private string _gameboxID;

    public string GameboxID
    {
        get { return _gameboxID; }
        set { _gameboxID = value; }
    }

    [SerializeField]
    private bool _isInitialed;

    public bool IsInitialed
    {
        get { return _isInitialed; }
        set { _isInitialed = value; }
    }

    [SerializeField]
    private string _systemDate;

    public string SystemDate
    {
        get { return _systemDate; }
        set { _systemDate = value; }
    }

    [SerializeField]
    private string _socketID;

    public string SocketID
    {
        get { return _socketID; }
        set { _socketID = value; }
    }

    [SerializeField]
    private string _passOperator;

    public string PassOperator
    {
        get { return _passOperator; }
        set { _passOperator = value; }
    }

    [SerializeField]
    private string _passOwner;

    public string PassOwner
    {
        get { return _passOwner; }
        set { _passOwner = value; }
    }

    [SerializeField]
    private string _passTechnician;

    public string PassTechnician
    {
        get { return _passTechnician; }
        set { _passTechnician = value; }
    }

    [SerializeField]
    private string _passWarehouse;

    public string PassWarehouse
    {
        get { return _passWarehouse; }
        set { _passWarehouse = value; }
    }
    [SerializeField]
    private string _machineVersion;

    public string MachineVersion
    {
        get { return _machineVersion; }
        set { _machineVersion = value; }
    }
    [SerializeField]
    private string _wareHouseID;

    public string WareHouseID
    {
        get { return _wareHouseID; }
        set { _wareHouseID = value; }
    }
    [SerializeField]
    private string _sumMachineSelled;

    public string SumMachineSelled
    {
        get { return _sumMachineSelled; }
        set { _sumMachineSelled = value; }
    }
    [SerializeField]
    private string _phoneNumber;

    public string PhoneNumber
    {
        get { return _phoneNumber; }
        set { _phoneNumber = value; }
    }
    [SerializeField]
    private string _checksum;

    public string CheckSum
    {
        get { return _checksum; }
        set { _checksum = value; }
    }
    [SerializeField]
    private string _userID;

    public string UserID
    {
        get { return _userID; }
        set { _userID = value; }
    }


    

    private void Start()
    {
        Bg = gameObject.GetComponentInChildren<Image>();
        Text gameBoxText = GetComponentInChildren<Text>();
        gameBoxText.text = _gameboxID;


        SKIO.socket.On("ROOMDROP", (SocketIOEvent e) =>
        {

            string roomID = e.data["rooms"][0].ToString().Trim('\"');
            Debug.Log(roomID);
            if (roomID == _socketID)
            {
                Destroy(this.gameObject);
            }
        });
    }
    private void Update()
    {
        

        //gameBoxText.text = GameBoxID;
        if (_isInitialed)
            Bg.color = InitializedColor;
        else
            Bg.color = yetInitializedColor;
       
    }

    public void Picked()
    {
        Datalogger datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();
        datalogger.GameboxID = _gameboxID;
        datalogger.IsInitialed = _isInitialed;
        datalogger.SystemDate = _systemDate;
        datalogger.PassOperator = _passOperator;
        datalogger.PassOwner = _passOwner;
        datalogger.PassTechnician = _passTechnician;
        datalogger.PassWarehouse = _passWarehouse;
        datalogger.SocketID = _socketID;

        datalogger.MachineVersion = _machineVersion;
        datalogger.WareHouseID = _wareHouseID;
        datalogger.SumMachineSelled = _sumMachineSelled;
        datalogger.PhoneNumber = _phoneNumber;
        datalogger.CheckSum = _checksum;
        datalogger.UserID = _userID;


        if (IsInitialed)
        {
            SceneManager.LoadScene("Login");

        }
        else
        {
            SceneManager.LoadScene("FirsttimeSetup");

        }


    }


}
