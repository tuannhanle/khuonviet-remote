﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FindByPhone : MonoBehaviour {
    [SerializeField]
    GameObject FindbyPhone;
    //Datalogger datalogger;
    public Text _phone;
    
	// Use this for initialization
	void Start () {
        //datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();
        _phone.text = GeneratorGamebox.FindByPhone;
    }
    public void ButtonClicked()
    {
        GameObject[] gameboxs = GameObject.FindGameObjectsWithTag("gamebox");
        foreach (GameObject item in gameboxs)
        {
            Destroy(item);
        }
       
        GeneratorGamebox.ListRoomGame.Clear();//Xoá hết ds game để render lại,nếu không bị trùng không render được
        //datalogger.FindByPhone = _phone.text;
        GeneratorGamebox.FindByPhone = _phone.text;
        StartCoroutine(ScanRoom());
        FindbyPhone.SetActive(false);

    }
    public void BackFintByPhoneClicked()
    {
        FindbyPhone.SetActive(false);

    }
    IEnumerator ScanRoom()
    {
        yield return new WaitForSeconds(0.1f);
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["gameboxID"] = "warehouse";

        if (SKIO.socket != null)
        {
            SKIO.socket.Emit("SCANROOM", new JSONObject(data));
        }
    }
}
