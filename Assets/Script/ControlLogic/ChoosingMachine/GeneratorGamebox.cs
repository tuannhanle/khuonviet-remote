﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIO;
using System;


public class GeneratorGamebox : MonoBehaviour {
    public GameObject gameBoxSample;
    public static string FindByPhone;
    Datalogger datalogger;
    public static ArrayList ListRoomGame = new ArrayList();

	// Use this for initialization
	void Start () {
        datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();
        SKIO.socket.On("ROOMCREATED", (SocketIOEvent e) =>
        {
            
            string socketid = e.data["socketid"].ToString().Trim('\"');
            string gameboxID = e.data["gameboxID"].ToString().Trim('\"');
            string isInitialed = e.data["isInitialed"].ToString().Trim('\"');
            string systemDate = e.data["systemDate"].ToString().Trim('\"');
            string passOperator = e.data["passOperator"].ToString().Trim('\"');
            string passOwner = e.data["passOwner"].ToString().Trim('\"');
            string passTechnician = e.data["passTechnician"].ToString().Trim('\"');
            string passWarehouse = e.data["passWarehouse"].ToString().Trim('\"');

            string MA_gameVersion = e.data["MA_gameVersion"].ToString().Trim('\"');
            string MA_warehouseNumber = e.data["MA_warehouseNumber"].ToString().Trim('\"');
            string MA_machineSellNumber = e.data["MA_machineSellNumber"].ToString().Trim('\"');
            string MA_phoneNumber = e.data["MA_phoneNumber"].ToString().Trim('\"');
            string MA_checksumNumber = e.data["MA_checksumNumber"].ToString().Trim('\"');
            int temp = ListRoomGame.IndexOf(socketid);
            //Debug.Log(temp);
            if (temp < 0  ) { //nếu không tồn tại thì create
                ListRoomGame.Add(socketid);
                //Debug.Log("000");
                if(string.IsNullOrEmpty(FindByPhone) || FindByPhone==null){
                    //Debug.Log("111");
                    Generator(
                        gameboxID,
                        isInitialed,
                        systemDate,
                        passOperator,
                        passOwner,
                        passTechnician,
                        passWarehouse,
                        socketid,
                        MA_gameVersion,
                        MA_warehouseNumber,
                        MA_machineSellNumber,
                        MA_phoneNumber,
                        MA_checksumNumber
                        );
                }
                if(string.Equals(FindByPhone,MA_phoneNumber)){
                    //Debug.Log("222");
                    Generator(
                        gameboxID,
                        isInitialed,
                        systemDate,
                        passOperator,
                        passOwner,
                        passTechnician,
                        passWarehouse,
                        socketid,
                        MA_gameVersion,
                        MA_warehouseNumber,
                        MA_machineSellNumber,
                        MA_phoneNumber,
                        MA_checksumNumber
                        );
                }
            }
            
            
        });

    }
	
	// Update is called once per frame
	void Update () {
		
	}
    void Generator(
        string gameboxID,      string isInitialed,
        string systemDate,     string passOperator,         string passOwner,
        string passTechnician, string passWarehouse,        string socketid,
        string MA_gameVersion, string MA_warehouseNumber,   string MA_machineSellNumber, 
        string MA_phoneNumber, string MA_checksumNumber)
    {
        GameObject gameBox = Instantiate(gameBoxSample,this.transform);
        GameboxPicked GP = gameBox.GetComponent<GameboxPicked>();
        GP.GameboxID = gameboxID;
        GP.SystemDate = systemDate;
        GP.PassOperator = passOperator;
        GP.PassOwner = passOwner;
        GP.PassTechnician = passTechnician;
        GP.PassWarehouse = passWarehouse;

        GP.MachineVersion = MA_gameVersion;
        GP.WareHouseID = MA_warehouseNumber;
        GP.SumMachineSelled = MA_machineSellNumber;
        GP.PhoneNumber = MA_phoneNumber;
        GP.CheckSum = MA_checksumNumber;
        
        

        GP.SocketID = socketid;

        Debug.Log(GP.IsInitialed);
        if (isInitialed == "True")
        {
            GP.IsInitialed = true;

        }
        else
        {
            GP.IsInitialed = false;

        }

    }
}
