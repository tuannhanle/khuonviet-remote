﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WarehouseSetup : MonoBehaviour {

    public GameObject prefabLogout;
    public GameObject prefabResetpassword;
    Datalogger datalogger;
    public Text VersionNumber;
    public Text GameBoxId;

	// Use this for initialization
	void Start () {
        datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();
        GameBoxId.text = datalogger.GameboxID;
        VersionNumber.text = datalogger.MachineVersion;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void CustomerButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        SceneManager.LoadScene("CustomerSetup");
    }
    public void ResetOwnerPasswordButtonClicked()
    {
        //SceneManager.LoadScene("");
        Sound.Singleton.PlaySoundButton();
        ResetPassord.UserId = "Owner";
        prefabResetpassword.SetActive(true);
    }
     public void ResetTechnicianPasswordButtonClicked()
    {
        //SceneManager.LoadScene("");
        Sound.Singleton.PlaySoundButton();

        ResetPassord.UserId = "Technician";
        prefabResetpassword.SetActive(true);
    }
    public void ChangeWarehouseButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        SceneManager.LoadScene("ChangePassword");
    }
    public void BookkeepingButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        SceneManager.LoadScene("Bookkeeping");
    }
    public void SetPercentButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        SceneManager.LoadScene("SetPercent");

    }
    public void LogoutButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        prefabLogout.SetActive(true);

    }

    public void BackButtonClicked()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
