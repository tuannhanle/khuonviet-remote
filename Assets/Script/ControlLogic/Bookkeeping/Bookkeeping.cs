﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using SocketIO;

public class Bookkeeping : MonoBehaviour
{
    public GameObject prefabLogout;

    [SerializeField]
    private Text _GETduration, _GETsysdate,_GETenddate;

    [SerializeField]
    private Text _duration;
    [SerializeField]
    private Text _systemdate;
    [SerializeField]
    private GameObject Infomation;
    [SerializeField]
    private GameObject Placeholder;
    [SerializeField]
    private GameObject Calender;
    Datalogger datalogger;
    Dictionary<string, string> parameter = new Dictionary<string, string>();
    private enum BOOKKEEPING
    {
        GetFromSV,
        SetToSV
    }
    private void Awake()
    {
        datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();

    }
    private void Start()
    {
        ConvertParameterToJSON("", "", datalogger.SocketID);
        StartCoroutine(BookkeepingSocket(_bookkeeping: BOOKKEEPING.GetFromSV));
        SKIO.socket.On("DEBOOKKEEPING",(SocketIOEvent e) =>
        {
            _GETduration.text = e.data["duration"].ToString().Split('\"')[1];
            _GETsysdate.text = new System.DateTime(Convert.ToInt64(e.data["systemdate"].ToString().Split('\"')[1])).ToString("dd/MM/yyyy HH:mm:ss");
            Debug.Log(e.data["GetRemainRuntime"].ToString());
        });
        SKIO.socket.On("BOOKKEEPINGDONE", (SocketIOEvent e) =>
        {
            _GETduration.text = _duration.text;
            _GETsysdate.text = _systemdate.text;
            Debug.Log("BOOKKEEPINGDONE");
        });
    }
    public void BackButtonClicked()
    {
        //string userId = datalogger.UserID;
        //switch (userId)
        //{
        //    case "Operator":
        //        SceneManager.LoadScene("OperatorSetup");
        //        break;
        //    case "Owner":
        //        SceneManager.LoadScene("OwnerSetup");
        //        break;
        //    case "Technician":
        //        SceneManager.LoadScene("TechnicianSetup");
        //        break;
        //    case "Warehouse":
        //        SceneManager.LoadScene("WarehouseSetup");
        //        break;
        //    default:
        //        break;
        //}
        Sound.Singleton.PlaySoundButton();

        prefabLogout.SetActive(true);
    }
    public void InfoButtonClicked()
    {
        Infomation.active = !Infomation.active;
    }
    public void SystemDateClicked()
    {
        Placeholder.active = false;
        Calender.active = true;
    }

    public void ConfirmButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        if (_systemdate.text != "" && _duration.text != "")
        {
            ConvertParameterToJSON(
                    duration: _duration.text,
                    systemdate: _systemdate.text,
                    socketid: datalogger.SocketID);

            StartCoroutine(
                BookkeepingSocket(
                    _bookkeeping: BOOKKEEPING.SetToSV

            ));
        }

    }
    void ConvertParameterToJSON(string duration, string systemdate, string socketid)
    {
        parameter["duration"] = duration;
        parameter["systemdate"] = systemdate;
        parameter["socketid"] = socketid;


    }
    IEnumerator BookkeepingSocket(BOOKKEEPING _bookkeeping)
    {
        yield return new WaitForSeconds(0.1f);

        if (SKIO.socket != null && _bookkeeping == BOOKKEEPING.GetFromSV)
        {
            SKIO.socket.Emit("ENBOOKKEEPING", new JSONObject(parameter));

        }
        if (SKIO.socket != null && _bookkeeping == BOOKKEEPING.SetToSV)
        {
            SKIO.socket.Emit("BOOKKEEPING", new JSONObject(parameter));

        }
    }

}
