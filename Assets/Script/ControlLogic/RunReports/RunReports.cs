﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RunReports : MonoBehaviour {

    public GameObject prefabLogout;
    public Text VersionNumber;
    public Text GameBoxId;

    Datalogger datalogger;

    
	// Use this for initialization
	void Start () {
        datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();
        GameBoxId.text = datalogger.GameboxID;
        VersionNumber.text = datalogger.MachineVersion;
    }

    public void ShiftChangeButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        SceneManager.LoadScene("Report");

    }
    
    public void DailyButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();
        //CalenderPannel.SetActive(true);
        SceneManager.LoadScene("ReportDaily");
    }
    public void WeeklyButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();
        //CalenderPannel.SetActive(true);
        SceneManager.LoadScene("ReportWeekly");
    }
    public void MonthlyButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        SceneManager.LoadScene("ReportMonthly");
    }
    public void CustomButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        //SceneManager.LoadScene("");
    }
    public void LogoutButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        prefabLogout.SetActive(true);

    }
    public void BackButtonClicked()
    {
        string userId = datalogger.UserID;
        switch (userId)
        {
            case "Operator":
                SceneManager.LoadScene("OperatorSetup");
                break;
            case "Owner":
                SceneManager.LoadScene("OwnerSetup");
                break;
            case "Technician":
                SceneManager.LoadScene("TechnicianSetup");
                break;
            case "Warehouse":
                SceneManager.LoadScene("WarehouseSetup");
                break;
            default:
                break;
        }
    }
	// Update is called once per frame
	void Update () {
		
	}
}
