﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using SocketIO;
using System;

public class SetWarehouse : MonoBehaviour
{

    public GameObject prefabLogout;
    public Text WarehouseNumber, VersionNumber, GameBoxId;
    public string error = "Error : Warehouse number must 2 char";
    public Text Error;
    Datalogger datalogger;
    // Use this for initialization
    void Start()
    {
        datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();
        GameBoxId.text = datalogger.GameboxID;
        VersionNumber.text = datalogger.MachineVersion;

        SKIO.socket.On("WAREHOUSESETTUPDONE", (SocketIOEvent e) =>
        {
            Debug.Log(string.Format("WAREHOUSESETTUPDONE:[name: {0}, data: {1}]", e.name, e.data));
            Error.text = "Set Warehouse Number Successfully ";
            GameBoxId.text = datalogger.GameboxID;
        });
    }
    public void SetWarehouseButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        string _warehouseNumber = WarehouseNumber.text;
        Error.text = "";
        if (Int32.Parse(_warehouseNumber) < 4)
        {
            _warehouseNumber = "0" + _warehouseNumber;
            Debug.Log("SetWarehouseButtonClicked");
            string _machineVersion = datalogger.MachineVersion;
            string _sellNumber = datalogger.SumMachineSelled;
            string _phoneNumber = datalogger.PhoneNumber;
            string _checksum = SupportLibrary.CalcCheckDigit(_machineVersion + _warehouseNumber + _sellNumber + _phoneNumber).ToString();
            string _gameboxID = _machineVersion + _warehouseNumber + _checksum + _sellNumber + _phoneNumber;
            //Set lai cho dataloger
            datalogger.GameboxID = _gameboxID;
            datalogger.MachineVersion = _machineVersion;
            datalogger.WareHouseID = _warehouseNumber;
            datalogger.SumMachineSelled = _sellNumber;
            datalogger.PhoneNumber = _phoneNumber;
            datalogger.CheckSum = _checksum;
            //Emit
            StartCoroutine(EmitSetWarehouse());
        }
        else
        {
            Error.text = error;
        }
    }
    public void BackButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        SceneManager.LoadScene("CustomerSetup");
        //Sound.Singleton.PlaySoundButton();

        //prefabLogout.SetActive(true);
    }
    // Update is called once per frame
    void Update()
    {

    }
    IEnumerator EmitSetWarehouse()
    {
        yield return new WaitForSeconds(0.1f);
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["socketid"] = datalogger.SocketID;
        data["gameboxID"] = datalogger.GameboxID;
        data["MA_gameVersion"] = datalogger.MachineVersion;
        data["MA_warehouseNumber"] = datalogger.WareHouseID;
        data["MA_machineSellNumber"] = datalogger.SumMachineSelled;
        data["MA_phoneNumber"] = datalogger.PhoneNumber;
        data["MA_checksumNumber"] = datalogger.CheckSum;

        if (SKIO.socket != null)
        {

            SKIO.socket.Emit("WAREHOUSESETTUP", new JSONObject(data));
        }
    }

}
