﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CustomerSettup : MonoBehaviour {

    Datalogger datalogger;
    public Text VersionNumber;
    public Text GameBoxId;
    void Start () {
        datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();
        GameBoxId.text = datalogger.GameboxID;
        VersionNumber.text = datalogger.MachineVersion;
    }
    public void PhoneButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        SceneManager.LoadScene("SetPhone");
    }
    public void WareHouseButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        SceneManager.LoadScene("SetWarehouse");
    }
    public void SellButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        SceneManager.LoadScene("SetSell");
    }
    public void BackButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        string userId = datalogger.UserID;
        switch (userId)
        {
            case "Operator":
                SceneManager.LoadScene("OperatorSetup");
                break;
            case "Owner":
                SceneManager.LoadScene("OwnerSetup");
                break;
            case "Technician":
                SceneManager.LoadScene("TechnicianSetup");
                break;
            case "Warehouse":
                SceneManager.LoadScene("WarehouseSetup");
                break;
            default:
                break;
        }
    }
	// Update is called once per frame
	void Update () {
		
	}
}
