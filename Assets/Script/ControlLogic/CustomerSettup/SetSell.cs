﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using SocketIO;

public class SetSell : MonoBehaviour
{

    public GameObject prefabLogout;
    public Text SellNumber, VersionNumber, GameBoxId;
    public string error = "Error : Sell number must 4 char";
    public Text Error;
    Datalogger datalogger;
    // Use this for initialization
    void Start()
    {
        datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();
        GameBoxId.text = datalogger.GameboxID;
        VersionNumber.text = datalogger.MachineVersion;

        SKIO.socket.On("SELLSETTUPDONE", (SocketIOEvent e) =>
        {
            Debug.Log(string.Format("SELLSETTUPDONE:[name: {0}, data: {1}]", e.name, e.data));
            Error.text = "Set Sell Number Successfully ";
            GameBoxId.text = datalogger.GameboxID;
        });
    }
    public void SetSellButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        string _sellNumber = SellNumber.text;
        Error.text = "";
        if (_sellNumber.Length == 4)
        {

            Debug.Log("SetSellButtonClicked");
            string _machineVersion = datalogger.MachineVersion;
            string _wareHouseID = datalogger.WareHouseID;
            string _phoneNumber = datalogger.PhoneNumber;
            string _checksum = SupportLibrary.CalcCheckDigit(_machineVersion + _wareHouseID + _sellNumber + _phoneNumber).ToString();
            string _gameboxID = _machineVersion + _wareHouseID + _checksum + _sellNumber + _phoneNumber;
            //Set lai cho dataloger
            datalogger.GameboxID = _gameboxID;
            datalogger.MachineVersion = _machineVersion;
            datalogger.WareHouseID = _wareHouseID;
            datalogger.SumMachineSelled = _sellNumber;
            datalogger.PhoneNumber = _phoneNumber;
            datalogger.CheckSum = _checksum;
            //Emit
            StartCoroutine(EmitSetSell());
        }
        else
        {
            Error.text = error;
        }
    }
    public void BackButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        SceneManager.LoadScene("CustomerSetup");
        //Sound.Singleton.PlaySoundButton();

        //prefabLogout.SetActive(true);
    }
    // Update is called once per frame
    void Update()
    {

    }
    IEnumerator EmitSetSell()
    {
        yield return new WaitForSeconds(0.1f);
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["socketid"] = datalogger.SocketID;
        data["gameboxID"] = datalogger.GameboxID;
        data["MA_gameVersion"] = datalogger.MachineVersion;
        data["MA_warehouseNumber"] = datalogger.WareHouseID;
        data["MA_machineSellNumber"] = datalogger.SumMachineSelled;
        data["MA_phoneNumber"] = datalogger.PhoneNumber;
        data["MA_checksumNumber"] = datalogger.CheckSum;

        if (SKIO.socket != null)
        {

            SKIO.socket.Emit("SELLSETTUP", new JSONObject(data));
        }
    }

}
