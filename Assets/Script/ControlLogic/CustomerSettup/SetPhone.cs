﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using SocketIO;

public class SetPhone : MonoBehaviour {

    public GameObject prefabLogout;

    public Text Phone, VersionNumber, GameBoxId;
    public string error = "Error : Phone number incorrect";
    public Text Error;
    Datalogger datalogger;
	// Use this for initialization
    void Start()
    {
        datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();
        GameBoxId.text = datalogger.GameboxID;
        VersionNumber.text = datalogger.MachineVersion;

        SKIO.socket.On("PHONESETTUPDONE", (SocketIOEvent e) =>
        {
            Debug.Log(string.Format("PHONESETTUPDONE:[name: {0}, data: {1}]", e.name, e.data));
            Error.text = "Set Phone Successfully ";
            GameBoxId.text = datalogger.GameboxID;

        });
    }
    public void SetPhoneButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        string _phoneNumber = Phone.text;
        Error.text = "";
        if (_phoneNumber.Length ==10)
        {
            
            Debug.Log("SetPhoneButtonClicked");
            string _machineVersion = datalogger.MachineVersion;
            string _wareHouseID = datalogger.WareHouseID;
            string _sumMachineSelled = datalogger.SumMachineSelled;
            string _checksum = SupportLibrary.CalcCheckDigit(_machineVersion + _wareHouseID + _sumMachineSelled + _phoneNumber).ToString();
            string _gameboxID = _machineVersion + _wareHouseID + _checksum + _sumMachineSelled + _phoneNumber;
            //Set lai cho dataloger
            datalogger.GameboxID = _gameboxID;
            datalogger.MachineVersion = _machineVersion;
            datalogger.WareHouseID = _wareHouseID;
            datalogger.SumMachineSelled = _sumMachineSelled;
            datalogger.PhoneNumber = _phoneNumber;
            datalogger.CheckSum = _checksum;
            //Emit
     
            StartCoroutine(EmitSetPhone());
        }
        else 
        {
            Error.text = error;
        }
    }
    public void BackButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        SceneManager.LoadScene("CustomerSetup");
        //Sound.Singleton.PlaySoundButton();

        //prefabLogout.SetActive(true);
    }
	// Update is called once per frame
	void Update () {
		
	}
    IEnumerator EmitSetPhone()
    {
        yield return new WaitForSeconds(0.1f);
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["socketid"] = datalogger.SocketID;
        data["gameboxID"] = datalogger.GameboxID;
        data["MA_gameVersion"] = datalogger.MachineVersion;
        data["MA_warehouseNumber"] = datalogger.WareHouseID;
        data["MA_machineSellNumber"] = datalogger.SumMachineSelled;
        data["MA_phoneNumber"] = datalogger.PhoneNumber;
        data["MA_checksumNumber"] = datalogger.CheckSum;
       
        if (SKIO.socket != null)
        {

            SKIO.socket.Emit("PHONESETTUP", new JSONObject(data));
        }
    }
    
}
