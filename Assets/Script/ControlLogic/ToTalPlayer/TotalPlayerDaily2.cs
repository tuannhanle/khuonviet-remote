﻿using SocketIO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class TotalPlayerDaily2 : MonoBehaviour
{
    [SerializeField]
    private Text createAt,playerNum;
    private int MaxNumPlayer = 20;
    RectTransform myTransform;
    public GameObject playerBoxSample;
    public GameObject CalenderPannel;
    public GameObject[] PlayerBoxs;
    Datalogger datalogger;
    Dictionary<string, string> parameter = new Dictionary<string, string>();
    private float _timecounter = 0;

    private void Awake()
    {
        try
        {
            datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();
            parameter["socketid"] = datalogger.SocketID;
        }
        catch (System.Exception)
        {


        }

    }
    void Start()
    {
        myTransform = this.GetComponent<RectTransform>();
        //StartCoroutine(EmitTotal());
        SKIO.socket.On("Khiem_GAME_SEND_TOTALDAILY2", (SocketIOEvent e) =>
        {
            Debug.Log(string.Format("Khiem_GAME_SEND_TOTALDAILY2:[name: {0}, data: {1}]", e.name, e.data));

            if (e.data["success"].ToString() == "true")
            {
                Debug.Log("true1");
                UpdateUI(e);
            }
            else
            {

                Debug.Log("false1");
                UpdateDefaultUI();
            }

        });
    }

    void UpdateDefaultUI()
    {
        this.PlayerBoxs = new GameObject[1];
        myTransform.sizeDelta = new Vector2(myTransform.sizeDelta.x,1500);   
        PlayerBoxs[0] = Generator("N/A", "N/A", "N/A");
    }


    void UpdateUI(SocketIOEvent e)
    {
        int sum = 0;
        int height = e.data.Count < 10 ? 1500 : (e.data.Count + 4) * 100;
        this.PlayerBoxs = new GameObject[e.data.Count - 1];
        
        myTransform.sizeDelta = new Vector2(myTransform.sizeDelta.x, height);   //Dai them 200px
        for (int i = 0; i != e.data.Count-2; ++i)
        {
            PlayerBoxs[i] = Generator(e.data[i]["timestamp"].ToString().Trim('\"'), e.data[i]["payment"].ToString().Trim('\"'), e.data[i]["changeType"].ToString().Trim('\"'));
            sum += Int32.Parse(e.data[i]["payment"].ToString().Trim('\"'));
        }
        PlayerBoxs[e.data.Count - 2] = Generator("Total",sum.ToString(),"");

    }
    GameObject Generator(string timestamp, string totalIn, string totalOut)
    {
        GameObject playerBox = Instantiate(playerBoxSample, this.transform);
        PlayerBox2 GP = playerBox.GetComponent<PlayerBox2>();
        GP.timestamp = timestamp;
        GP.totalIn = totalIn;
        GP.totalOut = totalOut;
        return playerBox;
    }
    public void BackButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        SceneManager.LoadScene("ToTalPlayerMenu");
    }
    public void InputDayButtonClicked()
    {
        CalenderPannel.SetActive(true);
    }
    public void OKButtonClicked()
    {
        if (PlayerBoxs != null && PlayerBoxs.Length > 0)
        {
            for (int i = 0; i < PlayerBoxs.Length; i++)
            {
                Destroy(PlayerBoxs[i]);
            }
        }
        StartCoroutine(EmitTotal());//in socket.On(getdatareport)
                                    //StartCoroutine(EmitReport());

    }
    IEnumerator EmitTotal()
    {
        yield return new WaitForSeconds(0.1f);

        //parameter["createAt"] = "6/7/2019";
        string[] temp = createAt.text.Split('/');
        parameter["gameboxID"] = datalogger.GameboxID;
        parameter["createAt"] = temp[0] + '/' + temp[1] + '/' + temp[2].Split(' ')[0];
        //parameter["createAt"] = "08/08/2019";

        parameter["socketid"] = datalogger.SocketID;
        parameter["playerNum"] = playerNum.text;


        //Debug.Log(temp[0] + '/' + temp[1] + '/' + temp[2].Split(' ')[0]);

        if (SKIO.socket != null)
        {
            SKIO.socket.Emit("Khiem_GET_TOTALDAILY2", new JSONObject(parameter));

        }

    }
}
