﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TotalPlayerMenu : MonoBehaviour
{

    public GameObject prefabLogout;
    public Text VersionNumber;
    public Text GameBoxId;

    Datalogger datalogger;


    // Use this for initialization
    void Start()
    {
        datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();
        GameBoxId.text = datalogger.GameboxID;
        VersionNumber.text = datalogger.MachineVersion;
    }

    public void CurrentButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        SceneManager.LoadScene("ToTalPlayer");

    }

    public void DailyButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();
        //CalenderPannel.SetActive(true);
        SceneManager.LoadScene("ToTalPlayerDaily");
    }
    public void Daily2ButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();
        //CalenderPannel.SetActive(true);
        SceneManager.LoadScene("ToTalPlayerDaily2");
    }
    public void BackButtonClicked()
    {
        string userId = datalogger.UserID;
        switch (userId)
        {
            case "Operator":
                SceneManager.LoadScene("OperatorSetup");
                break;
            case "Owner":
                SceneManager.LoadScene("OwnerSetup");
                break;
            case "Technician":
                SceneManager.LoadScene("TechnicianSetup");
                break;
            case "Warehouse":
                SceneManager.LoadScene("WarehouseSetup");
                break;
            default:
                break;
        }
    }
    // Update is called once per frame
    void Update()
    {

    }
}
