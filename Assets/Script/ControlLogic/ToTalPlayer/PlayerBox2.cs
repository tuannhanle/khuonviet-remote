﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBox2 : MonoBehaviour {

    Image Bg;
    public Text timestampText;
    public Text totalInText;
    public Text totalOutText;

    [SerializeField]
    private string _timestamp;

    public string timestamp
    {
        get { return _timestamp; }
        set { _timestamp = value; }
    }
    [SerializeField]
    private string _totalIn;

    public string totalIn
    {
        get { return _totalIn; }
        set { _totalIn = value; }
    }
    [SerializeField]
    private string _totalOut;

    public string totalOut
    {
        get { return _totalOut; }
        set { _totalOut = value; }
    }
    private void Start()
    {
        Bg = gameObject.GetComponentInChildren<Image>();
        //Text playerIdxText = GetComponentInChildren<Text>();
        //Text totalInText = GetComponentInChildren<GameObject>().GetComponentInChildren<Text>();
        //Text totalOutText = GetComponentInChildren<GameObject>().GetComponentInChildren<Text>();
        timestampText.text = _timestamp;
        totalInText.text = _totalIn;
        totalOutText.text = _totalOut;
    }
}
