﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using SocketIO;

public class PlayerBox : MonoBehaviour
{
    Image Bg;
    public Text playerIdxText;
    public Text totalInText;
    public Text totalOutText;

    [SerializeField]
    private string _playerIdx;

    public string playerIdx
    {
        get { return _playerIdx; }
        set { _playerIdx = value; }
    }
    [SerializeField]
    private string _totalIn;

    public string totalIn
    {
        get { return _totalIn; }
        set { _totalIn = value; }
    }
    [SerializeField]
    private string _totalOut;

    public string totalOut
    {
        get { return _totalOut; }
        set { _totalOut = value; }
    }
    private void Start()
    {
        Bg = gameObject.GetComponentInChildren<Image>();
        //Text playerIdxText = GetComponentInChildren<Text>();
        //Text totalInText = GetComponentInChildren<GameObject>().GetComponentInChildren<Text>();
        //Text totalOutText = GetComponentInChildren<GameObject>().GetComponentInChildren<Text>();
        playerIdxText.text = _playerIdx;
        totalInText.text = _totalIn;
        totalOutText.text = _totalOut;
    }


}
