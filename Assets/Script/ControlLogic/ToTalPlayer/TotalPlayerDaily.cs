﻿using SocketIO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class TotalPlayerDaily : MonoBehaviour
{
    [SerializeField]
    private Text createAt;
    private int MaxNumPlayer = 20;
    public GameObject playerBoxSample;
    public GameObject CalenderPannel;
    public GameObject[] PlayerBoxs = new GameObject[20];
    Datalogger datalogger;
    Dictionary<string, string> parameter = new Dictionary<string, string>();
    private float _timecounter = 0;

    private void Awake()
    {
        try
        {
            datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();
            parameter["socketid"] = datalogger.SocketID;
        }
        catch (System.Exception)
        {


        }

    }
    void Start()
    {
        SKIO.socket.On("Khiem_SEND_DAILYTOTAL", (SocketIOEvent e) =>
        {
            Debug.Log(string.Format("Khiem_SEND_DAILYTOTAL:[name: {0}, data: {1}]", e.name, e.data));

            if (e.data["success"].ToString() == "true")
            {
                Debug.Log("true1");
                UpdateUI(e);
            }
            else
            {

                Debug.Log("false1");
                UpdateDefaultUI();
            }

        });
    }
 
    void UpdateDefaultUI()
    {
        for (int i = 0; i != this.MaxNumPlayer; ++i)
        {
            
            PlayerBoxs[i] = Generator(i.ToString(), "N/A", "N/A");

        }
    }
    

    void UpdateUI(SocketIOEvent e)
    {
       
        for (int i = 0; i != this.MaxNumPlayer; ++i)
        {
            PlayerBoxs[i] = Generator(i.ToString(), e.data["PlayerTotalIn" + i.ToString()].ToString().Trim('\"'), e.data["PlayerTotalOut" + i.ToString()].ToString().Trim('\"'));

        }
    }
    GameObject Generator(string playerIdx, string totalIn, string totalOut)
    {
        GameObject playerBox = Instantiate(playerBoxSample, this.transform);
        PlayerBox GP = playerBox.GetComponent<PlayerBox>();
        GP.playerIdx = "Player " + playerIdx;
        GP.totalIn = totalIn;
        GP.totalOut = totalOut;
        return playerBox;
    }
    public void BackButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        SceneManager.LoadScene("ToTalPlayerMenu");
    }
    public void InputDayButtonClicked()
    {
        CalenderPannel.SetActive(true);
    }
    public void OKButtonClicked()
    {
        if (PlayerBoxs !=null && PlayerBoxs.Length > 0)
        {
            for (int i = 0; i < PlayerBoxs.Length; i++)
            {
                Destroy(PlayerBoxs[i]);
            }
        }
        StartCoroutine(EmitTotal());//in socket.On(getdatareport)
                                    //StartCoroutine(EmitReport());

    }
    IEnumerator EmitTotal()
    {
        yield return new WaitForSeconds(0.1f);

        //parameter["createAt"] = "6/7/2019";
        string[] temp = createAt.text.Split('/');
        parameter["gameboxID"] = datalogger.GameboxID;
        parameter["createAt"] = temp[0] + '/' + temp[1] + '/' + temp[2].Split(' ')[0];
        parameter["socketid"] = datalogger.SocketID;


        Debug.Log(temp[0] + '/' + temp[1] + '/' + temp[2].Split(' ')[0]);

        if (SKIO.socket != null)
        {
            SKIO.socket.Emit("Khiem_GET_DAILYTOTAL", new JSONObject(parameter));

        }

    }
}
