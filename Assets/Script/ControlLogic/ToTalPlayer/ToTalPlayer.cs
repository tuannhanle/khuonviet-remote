﻿using SocketIO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ToTalPlayer : MonoBehaviour
{
    private int MaxNumPlayer = 20;
    public GameObject playerBoxSample;
    Datalogger datalogger;
    Dictionary<string, string> parameter = new Dictionary<string, string>();
 

    private void Awake()
    {
        try
        {
            datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();
            parameter["socketid"] = datalogger.SocketID;
        }
        catch (System.Exception)
        {


        }

    }
    void Start()
    {
        StartCoroutine(EmitTotal());

        SKIO.socket.On("GETTOTALDONE", (SocketIOEvent e) =>
        {
            Debug.Log(string.Format("GETTOTALDONE:[name: {0}, data: {1}]", e.name, e.data));
            UpdateUI(e);
        });
    }
    private void Update()
    {
         
    }
    IEnumerator EmitTotal()
    {
        yield return new WaitForSeconds(0.1f);

        if (SKIO.socket != null)
        {
            SKIO.socket.Emit("GETTOTAL", new JSONObject(parameter));

        }

    }
    void UpdateUI(SocketIOEvent e)
    {
        for (int i = 0; i != this.MaxNumPlayer; ++i)
        {
            Generator(i.ToString(), e.data["PlayerTotalIn" + i.ToString()].ToString().Trim('\"'), e.data["PlayerTotalOut" + i.ToString()].ToString().Trim('\"'));
        }

    }
    void Generator(string playerIdx, string totalIn, string totalOut)
    {
        GameObject playerBox = Instantiate(playerBoxSample, this.transform);
        PlayerBox GP = playerBox.GetComponent<PlayerBox>();
        GP.playerIdx = "Player " + playerIdx;
        GP.totalIn = totalIn;
        GP.totalOut = totalOut;
    }
    public void BackButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();
        SceneManager.LoadScene("ToTalPlayerMenu");
    }

}
