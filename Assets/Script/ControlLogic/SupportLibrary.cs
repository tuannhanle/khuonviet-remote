﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;

public class SupportLibrary : MonoBehaviour {

    public static char CalcCheckDigit(string isbn)
    {
        char result = '?';

        //if (!string.IsNullOrWhiteSpace(isbn))
        //{
        isbn = isbn.Replace("-", "").Replace(" ", ""); // remove '-' and whitespaces

        if (Regex.IsMatch(isbn, @"^\d*$")) // is just numbers?
        {
            switch (isbn.Length)
            {
                case 19: //isbn 20
                    result = CalcCheckDigitForISBN21(isbn);
                    break;
            }
        }
        //}

        return result;
    }
    public static char CalcCheckDigitForISBN21(string isbn)
    {
        char result = '?';

        int sum = 0;
        bool oddIndex = false;

        foreach (char c in isbn)
        {
            int number = c - '0';
            sum += number * (oddIndex ? 3 : 1);
            oddIndex = !oddIndex;
        }

        result = (char)(((10 - sum % 10) % 10) + '0'); // number - '0' convert integer into char code
        return result;
    }
}
