﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefaultSetting : MonoBehaviour {
    private string _passewordDefault="1234";

    public string PasswordDefault
    {
        get { return _passewordDefault; }
        set { _passewordDefault = value; }
    }
    
}
