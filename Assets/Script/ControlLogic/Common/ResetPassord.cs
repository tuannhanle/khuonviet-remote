﻿using SocketIO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ResetPassord : MonoBehaviour {

    Datalogger datalogger;
    private static string _userId;
    public Text notification;
    public static string UserId
    {
        get { return _userId; }
        set { _userId = value; }
    }
    
	// Use this for initialization
    void Start()
    {
        datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();
        SKIO.socket.On("CHANGEPASSDONE", (SocketIOEvent e) =>
        {
            Debug.Log(string.Format("CHANGEPASSDONE:[name: {0}, data: {1}]", e.name, e.data));
            AddNotification(ResetPassord.UserId);
        });
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public void AddNotification(string userId)
    {
        this.notification.text = string.Format("Reset password for {0} success", userId);
    }
    public void YesButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        DefaultSetting dfs = new DefaultSetting();
        string _newPassword = dfs.PasswordDefault;
        string _userId = ResetPassord.UserId;//reset thep userID
        string _gameboxID = datalogger.GameboxID;
        string _passOperator = datalogger.PassOperator;
        string _passOwner = datalogger.PassOwner;
        string _passTechnician = datalogger.PassTechnician;
        string _passWarehouse = datalogger.PassWarehouse;
        string _socketid = datalogger.SocketID;
        switch (_userId)
        {
            case "Operator":
                datalogger.PassOperator = _newPassword;
                StartCoroutine(EmitChangePass(_userId,_gameboxID,_socketid,_newPassword,_passOwner,_passTechnician,_passWarehouse));
                //this.AddNotification(_userId);
                break;
            case "Owner":
                datalogger.PassOwner = _newPassword;
                StartCoroutine(EmitChangePass(_userId,_gameboxID,_socketid,_passOperator,_newPassword,_passTechnician,_passWarehouse));
               // this.AddNotification(_userId);
                break;
            case "Technician":
                datalogger.PassTechnician = _newPassword;
                StartCoroutine(EmitChangePass(_userId,_gameboxID,_socketid,_passOperator,_passOwner,_newPassword,_passWarehouse));
                //this.AddNotification(_userId);
                
                break;
            default:
                break;
        }
    }
    public void NoButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();
        this.notification.text = "";
        this.gameObject.SetActive(false);
    }
    IEnumerator EmitChangePass(string userId, string gameboxID, string _socketid, string _passOperator, string _passOwner, string _passTechnician, string _passWarehouse)
    {
        yield return new WaitForSeconds(0.1f);
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["gameboxID"] = gameboxID;
        data["socketid"] = _socketid;
        data["passOperator"] = _passOperator;
        data["passOwner"] = _passOwner;
        data["passTechnician"] = _passTechnician;
        data["passWarehouse"] = _passWarehouse;


        if (SKIO.socket != null)
        {
            SKIO.socket.Emit("CHANGEPASS", new JSONObject(data));
        }
    }
}
