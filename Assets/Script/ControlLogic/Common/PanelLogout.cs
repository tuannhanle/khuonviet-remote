﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PanelLogout : MonoBehaviour {

	// Use this for initialization
    
	void Start () {
		
	}
    public void YesButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        SceneManager.LoadScene("Login");
    }
    public void NoButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        this.gameObject.SetActive(false);
    }
   
	// Update is called once per frame
	void Update () {
		
	}
}
