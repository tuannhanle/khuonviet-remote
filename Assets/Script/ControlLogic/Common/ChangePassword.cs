﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using SocketIO;

public class ChangePassword : MonoBehaviour {

    public InputField oldPass, newPass,confirmPass;
    public GameObject error;
    public Text VersionNumber, GameBoxId;
    Datalogger datalogger;
    // Use this for initialization
	void Start () {
        datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();
        GameBoxId.text = datalogger.GameboxID;
        VersionNumber.text = datalogger.MachineVersion;
        SKIO.socket.On("CHANGEPASSDONE", (SocketIOEvent e) =>
        {
            Debug.Log(string.Format("CHANGEPASSDONE:[name: {0}, data: {1}]", e.name, e.data));
            error.GetComponent<Text>().text = "Change Password Successfully ";
        });
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public void BackButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        string userId = datalogger.UserID;
        switch (userId)
        {
            case "Operator":
                SceneManager.LoadScene("OperatorSetup");
                break;
            case "Owner":
                SceneManager.LoadScene("OwnerSetup");
                break;
            case "Technician":
                SceneManager.LoadScene("TechnicianSetup");
                break;
            case "Warehouse":
                SceneManager.LoadScene("WarehouseSetup");
                break;
            default:
                break;
        }
    }
    public void ChangeButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        string _oldPass = oldPass.text;
        string _newPass = newPass.text;
        string _confirmPass = confirmPass.text;
        string userId = datalogger.UserID;
        string _gameboxID = datalogger.GameboxID;
        string _passOperator = datalogger.PassOperator;
        string _passOwner = datalogger.PassOwner;
        string _passTechnician = datalogger.PassTechnician;
        string _passWarehouse = datalogger.PassWarehouse;
        string _socketid = datalogger.SocketID;
  
        if (ConfirmPassword(_newPass, _confirmPass))
        {
            switch (userId)
            {
                case "Technician":
                    if (ConfirmPassword(_oldPass, datalogger.PassTechnician))
                    {
                        datalogger.PassTechnician = _newPass;
                        StartCoroutine(
                            EmitChangePass(
                                userId:                     userId,
                                gameboxID:                _gameboxID, 
                                _socketid:                _socketid, 
                                _passOperator:            _passOperator,
                                _passOwner:               _passOwner, 
                                _passTechnician:          _newPass, 
                                _passWarehouse:           _passWarehouse));
                    }
                    else 
                    {
                        error.GetComponent<Text>().text = "Wrong Old Password";
                    }
                    break;
                case "Warehouse":
                    if (ConfirmPassword(_oldPass, datalogger.PassWarehouse))
                    {
                        datalogger.PassWarehouse = _newPass;
                        StartCoroutine(
                            EmitChangePass(
                                userId:                 userId,
                                gameboxID:              _gameboxID,
                                _socketid:              _socketid,
                                _passOperator:          _passOperator,
                                _passOwner:             _passOwner,
                                _passTechnician:        _passTechnician,
                                _passWarehouse:         _newPass));
                    }
                    else
                    {
                        error.GetComponent<Text>().text = "Wrong Old Password";
                    }
                    break;
                case "Owner":
                    if (ConfirmPassword(_oldPass, datalogger.PassOwner))
                    {
                        datalogger.PassOwner = _newPass;
                        StartCoroutine(
                            EmitChangePass(
                                userId:                 userId,
                                gameboxID:              _gameboxID,
                                _socketid:              _socketid,
                                _passOperator:          _passOperator,
                                _passOwner:             _newPass,
                                _passTechnician:        _passTechnician,
                                _passWarehouse:         _passWarehouse));
                    }
                    else
                    {
                        error.GetComponent<Text>().text = "Wrong Old Password";
                    }
                    break;
                case "Operator":
                    if (ConfirmPassword(_oldPass, datalogger.PassOperator))
                    {
                        datalogger.PassOperator = _newPass;
                        Debug.Log("Operator changepass");
                        StartCoroutine(
                            EmitChangePass(
                                userId:                 userId,
                                gameboxID:              _gameboxID,
                                _socketid:              _socketid,
                                _passOperator:          _newPass,
                                _passOwner:             _passOwner,
                                _passTechnician:        _passTechnician,
                                _passWarehouse:         _passWarehouse));
                    }
                    else
                    {
                        error.GetComponent<Text>().text = "Wrong Old Password";
                    }
                    break;

                default:
                    break;
            }
        }
        else 
        { 
            //set error
            error.GetComponent<Text>().text="New password and Confrim password not same";
        }
    }
    IEnumerator EmitChangePass(string userId,string gameboxID, string _socketid, string _passOperator, string _passOwner, string _passTechnician, string _passWarehouse)
    {
        yield return new WaitForSeconds(0.1f);
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["gameboxID"] = gameboxID;
        data["socketid"] = _socketid;
        data["passOperator"] = _passOperator;
        data["passOwner"] = _passOwner;
        data["passTechnician"] = _passTechnician;
        data["passWarehouse"] = _passWarehouse;


        if (SKIO.socket != null)
        {
            SKIO.socket.Emit("CHANGEPASS", new JSONObject(data));
            

        }
       

    }
    public bool ConfirmPassword(string pass1, string pass2)
    {
        if(pass1 == pass2)
            return true;
        return false;
    }

}
