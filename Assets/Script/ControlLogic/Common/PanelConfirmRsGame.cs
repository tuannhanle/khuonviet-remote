﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PanelConfirmRsGame : MonoBehaviour {
    Datalogger datalogger;

    // Use this for initialization
    void Start () {
        datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();

    }
    public void YesButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();
        StartCoroutine(ResetGame());
        //Destroy(GameObject.FindGameObjectWithTag("datalogger"));
        //Destroy(GameObject.FindGameObjectWithTag("socketioindex"));
        //Destroy(GameObject.FindGameObjectWithTag("sound"));
        //SKIO.socket.Close();
        //SceneManager.LoadScene("Gateway");
    }

    public void NoButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        this.gameObject.SetActive(false);
    }

    IEnumerator ResetGame()
    {
        yield return new WaitForSeconds(0.1f);
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["socketid"] = datalogger.SocketID;
        GeneratorGamebox.ListRoomGame.Clear();//Xoá hết ds game để render lại,nếu không bị trùng không render được
        if (SKIO.socket != null)
        {
            SKIO.socket.Emit("BACKPARAMETERSETTING", new JSONObject(data));
        }
    }

}
