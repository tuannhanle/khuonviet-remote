﻿using SocketIO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ExcuteCommand : MonoBehaviour {

    public Text _notification;
    public Text VersionNumber;
    public Text GameBoxId;
    Datalogger datalogger;
    Dictionary<string, string> data = new Dictionary<string, string>();

    // Use this for initialization
    void Start () {
        datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();
        GameBoxId.text = datalogger.GameboxID;
        VersionNumber.text = datalogger.MachineVersion;
        SKIO.socket.On("EXCUTECOMMANDDONE", (SocketIOEvent e) =>
        {
            Debug.Log(string.Format("EXCUTECOMMANDDONE:[name: {0}, data: {1}]", e.name, e.data));
            this._notification.text = "Cmd already run";
        });
    }
	
	public void ForgetAllWifiButtonClicked()
    {
        data["cmd"] = "cd /etc/NetworkManager/system-connections/; rm -r *";
        data["socketid"] = datalogger.SocketID;
        StartCoroutine(EmitCommand(data));
    }
    public void SetWifiButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();   
        SceneManager.LoadScene("SetWifi");
    }
    public void ChangeSocketButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();
        data["socketid"] = datalogger.SocketID;
        StartCoroutine(EmitChangeSocket(data));
    }
    public void BackButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        string userId = datalogger.UserID;
        switch (userId)
        {
            case "Operator":
                SceneManager.LoadScene("OperatorSetup");
                break;
            case "Owner":
                SceneManager.LoadScene("OwnerSetup");
                break;
            case "Technician":
                SceneManager.LoadScene("TechnicianSetup");
                break;
            case "Warehouse":
                SceneManager.LoadScene("WarehouseSetup");
                break;
            default:
                break;
        }
    }
    IEnumerator EmitCommand(Dictionary<string, string> data)
    {
        yield return new WaitForSeconds(0.1f);

        if (SKIO.socket != null)
        {
            SKIO.socket.Emit("EXCUTECOMMAND", new JSONObject(data));
        }
    }
    IEnumerator EmitChangeSocket(Dictionary<string, string> data)
    {
        yield return new WaitForSeconds(0.1f);

        if (SKIO.socket != null)
        {
            SKIO.socket.Emit("CHANGESOCKETIO", new JSONObject(data));
        }
    }
}
