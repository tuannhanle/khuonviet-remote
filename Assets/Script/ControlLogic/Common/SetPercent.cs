﻿using SocketIO;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SetPercent : MonoBehaviour {

    [SerializeField]
    private Text _notification, VersionNumber, GameBoxId;
    public InputField _Dat_Percent;

    Datalogger datalogger;
    Dictionary<string, string> data = new Dictionary<string, string>();
    private int MaxForWarehouse = 40;
    private int MaxForOwner = 20;

    // Use this for initialization
    void Start()
    {
        datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();
        GameBoxId.text = datalogger.GameboxID;
        VersionNumber.text = datalogger.MachineVersion;

        SKIO.socket.On("GETPERCENTDONE", (SocketIOEvent e) =>
        {
            Debug.Log(string.Format("GETPERCENTDONE:[name: {0}, data: {1}]", e.name, e.data));
            Debug.Log(e.data["Dat_Percent"].ToString().Trim('\"'));
            this._Dat_Percent.text = (float.Parse(e.data["Dat_Percent"].ToString().Trim('\"')) * 100).ToString();
            //this._notification.text = "Current Percent  : " + e.data["Dat_Percent"].ToString().Trim('\"');

        });
        SKIO.socket.On("SETPERCENTDONE", (SocketIOEvent e) =>
        {
            Debug.Log(string.Format("SETPERCENTDONE:[name: {0}, data: {1}]", e.name, e.data));
            this._notification.text = "Set Percent succsessfully";
        });


        //Emit get percent current
        data["socketid"] = datalogger.SocketID;
        StartCoroutine(EmitGetPercent(data));
    }
    public void ConfirmButtonClicked()
    {
        //percent 1->30%, kiem tra Int32.Parse(_Dat_Percent.text) > 20
        data["socketid"] = datalogger.SocketID;
        data["Dat_Percent"] = Math.Abs(Int32.Parse(_Dat_Percent.text)).ToString(); // lấy giá trị tuyệt đối
        if(datalogger.UserID == "Owner")
        {
            if(Int32.Parse(_Dat_Percent.text) <= MaxForOwner)
            {
                StartCoroutine(EmitSetPercent(data));

            }
            else
            {
                _notification.text = "Max Percent : " + MaxForOwner;
            }
        }
        if (datalogger.UserID == "Warehouse")
        {
            if (Int32.Parse(_Dat_Percent.text) <= MaxForWarehouse)
            {
                StartCoroutine(EmitSetPercent(data));
            }
            else
            {
                _notification.text = "Max Percent : " + MaxForWarehouse;
            }
        }

    }

    IEnumerator EmitSetPercent(Dictionary<string, string> data)
    {
        yield return new WaitForSeconds(0.1f);

        if (SKIO.socket != null)
        {
            SKIO.socket.Emit("SETPERCENT", new JSONObject(data));
        }


    }
    IEnumerator EmitGetPercent(Dictionary<string, string> data)
    {
        yield return new WaitForSeconds(0.1f);

        if (SKIO.socket != null)
        {
            SKIO.socket.Emit("GETPERCENT", new JSONObject(data));
        }


    }
    public void BackButtonClicked()
    {
        string userId = datalogger.UserID;
        switch (userId)
        {
            case "Operator":
                SceneManager.LoadScene("OperatorSetup");
                break;
            case "Owner":
                SceneManager.LoadScene("OwnerSetup");
                break;
            case "Technician":
                SceneManager.LoadScene("TechnicianSetup");
                break;
            case "Warehouse":
                SceneManager.LoadScene("WarehouseSetup");
                break;
            default:
                break;
        }
    }
}
