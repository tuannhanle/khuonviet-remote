﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using SocketIO;

public class SetWifi : MonoBehaviour {
    public GameObject prefabLogout;

    [SerializeField]
    private InputField ssid, pass, confirmpass;
    [SerializeField]
    private Text error, VersionNumber, GameBoxId;


    Datalogger datalogger;
    Dictionary<string, string> data = new Dictionary<string, string>();

    // Use this for initialization
    void Start () {
        datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();
        GameBoxId.text = datalogger.GameboxID;
        VersionNumber.text = datalogger.MachineVersion;

        SKIO.socket.On("SETWIFIDONE", (SocketIOEvent e) =>
        {
            Debug.Log(string.Format("SETWIFIDONE:[name: {0}, data: {1}]", e.name, e.data));
            error.text = "Set Wifi Successfully ";
        });
    }
    public void ConfirmButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        if (ssid.text!=""&&pass.text!=""&&confirmpass.text!="")
        {
            if (pass.text == confirmpass.text)
            {
                data["socketid"] = datalogger.SocketID;
                data["wifissid"] = ssid.text;
                data["wifipass"] = pass.text;
                //error.text = "Successfully";

                StartCoroutine(EmitSetWifi(data));
            }else
            {
                error.text = "Password is not the same";
            }
        }

    }

    IEnumerator EmitSetWifi(Dictionary<string, string>  data)
    {
        yield return new WaitForSeconds(0.1f);
        
        if (SKIO.socket != null)
        {
            SKIO.socket.Emit("SETWIFI", new JSONObject(data));
        }
    }

    public void BackButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();
        SceneManager.LoadScene("ExcuteCommand");

        //prefabLogout.SetActive(true);
    }
    public void ShutdownClicked()
    {
        //Sound.Singleton.PlaySoundButton();

        //string userId = datalogger.UserID;
        //switch (userId)
        //{
        //    case "Operator":
        //        SceneManager.LoadScene("OperatorSetup");
        //        break;
        //    case "Owner":
        //        SceneManager.LoadScene("OwnerSetup");
        //        break;
        //    case "Technician":
        //        SceneManager.LoadScene("TechnicianSetup");
        //        break;
        //    case "Warehouse":
        //        SceneManager.LoadScene("WarehouseSetup");
        //        break;
        //    default:
        //        break;
        //}
        Sound.Singleton.PlaySoundButton();

        prefabLogout.SetActive(true);
    }
}
