﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sound : MonoBehaviour {

    public AudioClip SoundButton;
    public static Sound Singleton;

	
	void Start () {
        Singleton = this;
        DontDestroyOnLoad(this);

	}
    public void PlaySoundButton()
    {
        this.GetComponent<AudioSource>().PlayOneShot(SoundButton);
    }
	// Update is called once per frame
	void Update () {
		
	}
}
