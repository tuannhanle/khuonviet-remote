﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Datalogger : MonoBehaviour {
    [SerializeField]
    private string _gameboxID;

    public string GameboxID
    {
        get { return _gameboxID; }
        set { _gameboxID = value; }
    }

    [SerializeField]
    private bool _isInitialed;

    public bool IsInitialed
    {
        get { return _isInitialed; }
        set { _isInitialed = value; }
    }

    [SerializeField]
    private string _systemDate;

    public string SystemDate
    {
        get { return _systemDate; }
        set { _systemDate = value; }
    }

    [SerializeField]
    private string _socketID;

    public string SocketID
    {
        get { return _socketID; }
        set { _socketID = value; }
    }

    [SerializeField]
    private string _passOperator;

    public string PassOperator
    {
        get { return _passOperator; }
        set { _passOperator = value; }
    }

    [SerializeField]
    private string _passOwner;

    public string PassOwner
    {
        get { return _passOwner; }
        set { _passOwner = value; }
    }

    [SerializeField]
    private string _passTechnician;

    public string PassTechnician
    {
        get { return _passTechnician; }
        set { _passTechnician = value; }
    }
    [SerializeField]
    private string _passWarehouse;

    public string PassWarehouse
    {
        get { return _passWarehouse; }
        set { _passWarehouse = value; }
    }
    [SerializeField]
    private string _machineVersion;

    public string MachineVersion
    {
        get { return _machineVersion; }
        set { _machineVersion = value; }
    }
    [SerializeField]
    private string _wareHouseID;

    public string WareHouseID
    {
        get { return _wareHouseID; }
        set { _wareHouseID = value; }
    }
    [SerializeField]
    private string _sumMachineSelled;

    public string SumMachineSelled
    {
        get { return _sumMachineSelled; }
        set { _sumMachineSelled = value; }
    }
    [SerializeField]
    private string _phoneNumber;

    public string PhoneNumber
    {
        get { return _phoneNumber; }
        set { _phoneNumber = value; }
    }

    [SerializeField]
    private string _checksum;

    public string CheckSum
    {
        get { return _checksum; }
        set { _checksum = value; }
    }

    [SerializeField]
    private string _userID;

    public string UserID
    {
        get { return _userID; }
        set { _userID = value; }
    }
    //[SerializeField]
    //private string _findbyphone;

    //public string FindByPhone
    //{
    //    get { return _findbyphone; }
    //    set { _findbyphone = value; }
    //}
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);

    }


}
