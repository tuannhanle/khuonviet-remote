﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FTSetup : MonoBehaviour {

    [SerializeField]
    private InputField _passText;
    [SerializeField]
    private Text versionNumber;
    [SerializeField]
    private Text machineID;
    // Use this for initialization
    void Start()
    {
        Datalogger datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();
        versionNumber.text = datalogger.GameboxID.Substring(0, 3);
        machineID.text = string.Format("{0} {1} {2} {3} {4}",
            datalogger.GameboxID.Substring(0, 3),
            datalogger.GameboxID.Substring(3, 2),
            datalogger.GameboxID.Substring(5, 1),
            datalogger.GameboxID.Substring(6, 4),
            datalogger.GameboxID.Substring(10)
            );


    }

    // Update is called once per frame
    void Update () {
		
	}
    public void NextButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        if (!string.IsNullOrEmpty(_passText.text) )
        {
            if(_passText.text == "1234")
            {
                SceneManager.LoadScene("InitialSetup");

            }
        }
    }
    public void BackStageClicked()
    {
        Destroy(GameObject.FindGameObjectWithTag("datalogger"));
        Destroy(GameObject.FindGameObjectWithTag("socketioindex"));
        Destroy(GameObject.FindGameObjectWithTag("sound"));

        SKIO.socket.Close();
        SceneManager.LoadScene("Gateway");

    }
}
