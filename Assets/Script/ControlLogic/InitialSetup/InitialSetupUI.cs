﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Text.RegularExpressions;


public class InitialSetupUI : MonoBehaviour {

    public InputField warehouseText, passwordText;
    public InputField sumMachineSelled;//Nhap so may da ban
    public InputField numberPhone;//Nhap so may da ban
    public GameObject CalenderPannel;
    private string _warehouseTextString, _passwordTextString;
    public GameObject systemDatePlaceholder;


    Datalogger datalogger;


    [SerializeField]
    private Text versionNumber;
    [SerializeField]
    private Text machineID;
    // Use this for initialization
    void Start()
    {

        //Default Value
        sumMachineSelled.text = "0000";
        numberPhone.text = "1234567890";
        warehouseText.text = "0";

        datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();
        versionNumber.text = datalogger.GameboxID.Substring(0, 3);
        machineID.text = string.Format("{0} {1} {2} {3} {4}",
            datalogger.GameboxID.Substring(0, 3),
            datalogger.GameboxID.Substring(3, 2),
            datalogger.GameboxID.Substring(5, 1),
            datalogger.GameboxID.Substring(6, 4),
            datalogger.GameboxID.Substring(10)
            );


    }

    // Update is called once per frame
    void Update ()
    {
        ProcessStringWareHouseID();
    }

    private void ProcessStringWareHouseID()
    {
        if (warehouseText.text.Length > 1)
        {
            warehouseText.text = warehouseText.text.Substring(0, 1);
        }

    }

    public void SystemDateButtonClick()
    {
        systemDatePlaceholder.SetActive(false);
        CalenderPannel.SetActive(true);
    }

    public void NextButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        if (CheckWarehouseTextString() && SetNewPassword() && isCheckLengthPhone() && isCheckLengthSelled())
        {

            //SendInitailMenu(); //?
            string _systemDate = datalogger.SystemDate;
            bool _isInitialed = true;
            string _passOperator = datalogger.PassOperator;
            string _passOwner = datalogger.PassOwner;
            string _passTechnician = datalogger.PassTechnician;
            string _passWarehouse = datalogger.PassWarehouse;
            string _socketid = datalogger.SocketID;

            string _machineVersion = datalogger.MachineVersion;
            
            string _wareHouseID =_warehouseTextString;
            string _sumMachineSelled = datalogger.SumMachineSelled;
            string _phoneNumber = datalogger.PhoneNumber;
            //Cal checksum       
            string _checksum = CalcCheckDigit(_machineVersion + _wareHouseID + _sumMachineSelled + _phoneNumber).ToString();
            Debug.Log("_checksum: " + _checksum);
            string gameboxID = _machineVersion + _wareHouseID + _checksum + _sumMachineSelled + _phoneNumber;
            Debug.Log("gameboxID: " + gameboxID);
            datalogger.GameboxID = gameboxID;
            datalogger.CheckSum = _checksum;

StartCoroutine(EmitFirstTimeSetup(gameboxID, _isInitialed, _socketid, _systemDate,
                _passOperator, _passOwner, _passTechnician, _passWarehouse, _machineVersion, _wareHouseID, _sumMachineSelled,
                _phoneNumber, _checksum));


        }
        

    }


    IEnumerator EmitFirstTimeSetup(string gameboxID, bool _isInitialed, string _socketid, string _systemDate,
        string _passOperator, string _passOwner, string _passTechnician, string _passWarehouse, string _machineVersion,
        string _wareHouseID,string _sumMachineSelled,string _phoneNumber,string _checksum)
    {
        yield return new WaitForSeconds(0.1f);
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["gameboxID"] = gameboxID;
        data["isInitialed"] = _isInitialed.ToString();
        data["socketid"] = _socketid;
        data["systemDate"] = _systemDate;
        data["passOperator"] = _passOperator;
        data["passOwner"] = _passOwner;
        data["passTechnician"] = _passTechnician;
        data["passWarehouse"] = _passWarehouse;

        data["MA_gameVersion"] = _machineVersion;
        data["MA_warehouseNumber"] = _wareHouseID;
        data["MA_machineSellNumber"] = _sumMachineSelled;
        data["MA_phoneNumber"] = _phoneNumber;
        data["MA_checksumNumber"] = _checksum;

        if (SKIO.socket != null)
        {
            SKIO.socket.Emit("FIRSTTIMESETUP", new JSONObject(data));

        }
        //Load scene Login
        SceneManager.LoadScene("Login");
    }
    private bool SetNewPassword()
    {
        if ((passwordText.text != datalogger.PassTechnician) && (passwordText.text.Length == 4))
        {
            datalogger.PassTechnician = passwordText.text;
            _passwordTextString = passwordText.text;
            return true;

        }
        else
        {
            return false;
        }
    }
    private bool isCheckLengthSelled()
    {
        if (sumMachineSelled.text.Length == 4)
        {
            datalogger.SumMachineSelled = sumMachineSelled.text;
            //_passwordTextString = passwordText.text;
            return true;

        }
        else
        {
            return false;
        }
    }
    private bool isCheckLengthPhone()
    {
        if (numberPhone.text.Length == 10)
        {
            datalogger.PhoneNumber = numberPhone.text;
            //_passwordTextString = passwordText.text;
            return true;

        }
        else
        {
            return false;
        }
    }

    private bool CheckWarehouseTextString()
    {
        if (warehouseText.text == "0" || warehouseText.text == "1" ||
           warehouseText.text == "2" || warehouseText.text == "3")
        {
            _warehouseTextString = "0"+warehouseText.text;
            return true;

        }
        else
        {
            return false;

        }
    }

    static char CalcCheckDigit(string isbn)
    {
        char result = '?';

        //if (!string.IsNullOrWhiteSpace(isbn))
        //{
        isbn = isbn.Replace("-", "").Replace(" ", ""); // remove '-' and whitespaces

        if (Regex.IsMatch(isbn, @"^\d*$")) // is just numbers?
        {
            switch (isbn.Length)
            {
                case 19: //isbn 20
                    result = CalcCheckDigitForISBN21(isbn);
                    break;
            }
        }
        //}

        return result;
    }

    private static char CalcCheckDigitForISBN21(string isbn)
    {
        char result = '?';

        int sum = 0;
        bool oddIndex = false;

        foreach (char c in isbn)
        {
            int number = c - '0';
            sum += number * (oddIndex ? 3 : 1);
            oddIndex = !oddIndex;
        }

        result = (char)(((10 - sum % 10) % 10) + '0'); // number - '0' convert integer into char code
        return result;
    }

}
