﻿using SocketIO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class CodeSetting : MonoBehaviour
{
    [SerializeField]
    private GameObject InformationObject;
    [SerializeField]
    private InputField InsertCoinScoreRatioField;
    [SerializeField]
    private InputField ArenaType_Field;
    [SerializeField]
    private InputField CodePrintDayField;
    [SerializeField]
    private InputField Dat_CodePrintDateTimeField;
    Dictionary<string, string> parameter = new Dictionary<string, string>();
    Datalogger datalogger;
    // Use this for initialization
    private void Awake()
    {
        try
        {
            datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();

        }
        catch (System.Exception)
        {


        }

    }
    void ConvertParameterToJSON()
    {

        parameter["socketid"] = datalogger.SocketID;


    }
    void Start()
    {
        SKIO.socket.On("DECODESETTING", (SocketIOEvent e) =>
        {
            Debug.Log(string.Format("DECODESETTING:[name: {0}, data: {1}]", e.name, e.data));
            InsertCoinScoreRatioField.text = e.data["InsertCoinScoreRatio"].ToString().Trim('\"');
            Debug.Log("DECODESETTINGDONE 1");

            ArenaType_Field.text = e.data["ArenaType_"].ToString().Trim('\"');
            Debug.Log("DECODESETTINGDONE 2");

            CodePrintDayField.text = e.data["CodePrintDay"].ToString().Trim('\"');
            Debug.Log("DECODESETTINGDONE 3");

            long x = long.Parse(e.data["Dat_CodePrintDateTime"].ToString().Trim('\"'));
            Debug.Log("DECODESETTINGDONE 4");

            DateTime myDate = new DateTime(x);
            Dat_CodePrintDateTimeField.text = myDate.ToString("MMMM dd, yyyy HH:mm:ss");
            Debug.Log("DECODESETTINGDONE");


        });
        ConvertParameterToJSON();
        StartCoroutine(EmitCodeSetting(parameter));
    }
    public void InfoButtonClick()
    {
        InformationObject.active = !InformationObject.active;
    }
    IEnumerator EmitCodeSetting(Dictionary<string, string> _parameter)
    {
        yield return new WaitForSeconds(0.1f);
        if (SKIO.socket != null)
        {
            SKIO.socket.Emit("CODESETTING", new JSONObject(_parameter));
        }
    }
    public void LeftHeaderButtonClicked()
    {
        SceneManager.LoadScene("TechnicianSetup");

    }
}
