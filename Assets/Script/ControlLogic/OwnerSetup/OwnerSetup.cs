﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OwnerSetup : MonoBehaviour {

    public GameObject prefabLogout;
    public GameObject prefabResetpassword;
    public Text VersionNumber;
    public Text GameBoxId;

    Datalogger datalogger;
    // Use this for initialization
    void Start() {
        datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();
        GameBoxId.text = datalogger.GameboxID;
        VersionNumber.text = datalogger.MachineVersion;
    }

    // Update is called once per frame
    void Update() {

    }
    public void SetZipCodeButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();

        SceneManager.LoadScene("ExcuteCommand");
    }
    public void GameParametersButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();
        SceneManager.LoadScene("ParameterSetting");
    }
    public void RunReportsClicked()
    {
        Sound.Singleton.PlaySoundButton();
        SceneManager.LoadScene("RunReports");
    }
    public void TotalPlayerClicked()
    {
        Sound.Singleton.PlaySoundButton();
        SceneManager.LoadScene("ToTalPlayerMenu");
    }
    public void SetPercentButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();
        SceneManager.LoadScene("SetPercent");

    }
    public void OperatorButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();
        //SceneManager.LoadScene("");
    }
    public void ResetOperatorButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();
        //SceneManager.LoadScene("");
    }
    public void ChangePasswordClicked()
    {
        Sound.Singleton.PlaySoundButton();
        SceneManager.LoadScene("ChangePassword");
    }
    public void LogoutButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();
        prefabLogout.SetActive(true);

    }
    public void BookkeepingButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();
        SceneManager.LoadScene("Bookkeeping");
    }
    public void BackButtonClicked()
    {
        Sound.Singleton.PlaySoundButton();
        SceneManager.LoadScene("MainMenu");
    }
}
