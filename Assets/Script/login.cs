﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;


[System.Serializable]
public class User
{
    public string id;
    public int per;
    public Boolean success;
    public static User CreateFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<User>(jsonString);
    }
}

public class login : MonoBehaviour {
    Datalogger datalogger;

    public GameObject id;
    public GameObject password;
    public GameObject wrongPassword;
    private string strid;
    private string strpassword;

    private void Start()
    {
        datalogger = GameObject.FindGameObjectWithTag("datalogger").GetComponent<Datalogger>();
    }
    // Use this for initialization
    public void RegisterButtonPressed()
    {
        Destroy(GameObject.FindGameObjectWithTag("datalogger"));
        Destroy(GameObject.FindGameObjectWithTag("socketioindex"));
        Destroy(GameObject.FindGameObjectWithTag("sound"));

        SKIO.socket.Close();
        Load("Gateway");

    }
    public void Load(string scenename)
    {
        Debug.Log("sceneName to load: " + scenename);
        SceneManager.LoadScene(scenename);
    }
    public void LoginButtonPressed()
    {
        //y tuong, tao dropdown de chon ID, sau do` pass theo ID.
        strid = id.GetComponent<Text>().text;
        strpassword = password.GetComponent<InputField>().text;
        if ( !string.IsNullOrEmpty(strpassword))
        {
            switch (strid)
            {
                case "Technician":
                    if (strpassword == datalogger.PassTechnician)
                    {
                        Load("TechnicianSetup");
                        datalogger.UserID = "Technician";
                    }
                    else
                    {
                        wrongPassword.SetActive(true);
                    }
                    break;
                case "Warehouse":
                    if (strpassword == datalogger.PassWarehouse)
                    {
                        datalogger.UserID = "Warehouse";
                        Load("WarehouseSetup");
                    }
                    else
                    {
                       
                        wrongPassword.SetActive(true);
                    }
                    break;
                case "Owner":
                    if (strpassword == datalogger.PassOwner)
                    {
                        datalogger.UserID = "Owner";
                        Load("OwnerSetup");
                    }
                    else
                    {
                        wrongPassword.SetActive(true);
                    }
                    break;
                case "Operator":
                    if (strpassword == datalogger.PassOperator)
                    {
                        datalogger.UserID = "Operator";
                        Load("OperatorSetup");
                    }
                    else
                    {
                        wrongPassword.SetActive(true);
                    }
                    break;
                    
                default:
                    break;
            }
        }
    }


    
}
