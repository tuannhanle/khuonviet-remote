﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Haddler_SendInitialMenu : MonoBehaviour {

    static public Haddler_SendInitialMenu instance;
    private void Awake()
    {
        instance = this;

    }
    public static void Handler(string _warehouseText, string _passwordText)
    {
        instance.StartCoroutine(instance.SendInitialMenu( _warehouseText,  _passwordText));


    }

    IEnumerator SendInitialMenu(string _warehouseText, string _passwordText)
    {
        yield return new WaitForSeconds(0.1f);
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["warehouseText"] = _warehouseText;
        data["passwordText"] = _passwordText;

        if (SKIO.socket != null)
        {
            SKIO.socket.Emit("SendInitialMenu", new JSONObject(data));
        }
    }
}
