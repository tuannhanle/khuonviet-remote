﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SocketIO;

public class SKIO : MonoBehaviour {


    public static  SocketIOComponent socket;
    public static bool trigger=false;

    private void Start()
    {
        
    }
    private void Awake()
    {
        socket = this.gameObject.GetComponent<SocketIOComponent>();
        DontDestroyOnLoad(socket);

    }

 
    private void Update()
    {
        if (trigger)
        {
            trigger = false;
            Debug.Log("trigger");
            //StartCoroutine(JoinHouse());
            //StartCoroutine(ScanRoom());
            SKIO.socket.On("connect", (SocketIOEvent e) =>
            {
                StartCoroutine(JoinHouse());
                StartCoroutine(ScanRoom());

            });
        }


    }


    IEnumerator JoinHouse()
    {
        yield return new WaitForSeconds(0.1f);



        if (socket != null)
        {
            socket.Emit("JOINHOUSE", new JSONObject());

        }
    }
    IEnumerator ScanRoom()
    {
        yield return new WaitForSeconds(0.1f);
        Dictionary<string, string> data = new Dictionary<string, string>();
        data["gameboxID"] = "warehouse";
        GeneratorGamebox.ListRoomGame.Clear();//Xoá hết ds game để render lại,nếu không bị trùng không render được
        if (socket != null)
        {
            socket.Emit("SCANROOM", new JSONObject(data));
        }
    }



}
