﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Define  {

    #region Scene Name

    public static string Scene_Store = "Store";
    public static string Scene_Login = "NewLogin";
    public static string Scene_MachineList = "MachineList";


    #endregion

    #region Network

    public static string NodeServerURL = "http://127.0.0.1:3000";

    #endregion

}
