﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class LoginController : MonoBehaviour {
    [SerializeField]
    InputField userNameInput, passWordInput;

    private void Start()
    {
        
    }

    public void LoginClicked()
    {
        string userName, passWord;
        userName = userNameInput.text;
        passWord = passWordInput.text;

        Debug.Log(userName+ passWord);

        StartCoroutine(Coro_Login(userName, passWord));

        //LoadSceen();

    }
    private IEnumerator Coro_Login(string strid, string strpassword)
    {
        Dictionary<string, string> passport = new Dictionary<string, string>();
        passport["username"] = strid;
        passport["password"] = strpassword;

        // Delete cookie before requesting a new login
        webservices.CookieString = null;

        var request = webservices.Post("login", new JSONObject(passport).ToString());
        yield return request.Send();

        if (request.isNetworkError)
        {
            Debug.LogError(request.error);
        }
        else
        {
            webservices.CookieString = request.GetResponseHeader("set-cookie");
            Debug.Log(webservices.CookieString);
            Debug.Log(request.downloadHandler.text);
            //if (String.Compare(request.downloadHandler.text, "OK") == 0) //so sanh ket qua tra ve == 0K => correct
            //{

            //LoadSceen();

            //}
        }
    }
    void LoadSceen()
    {
        SceneManager.LoadScene(Define.Scene_Store);
    }

}
