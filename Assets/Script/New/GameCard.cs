﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameCard : MonoBehaviour {
    [SerializeField]
    Image Image;
    [SerializeField]
    Text BigText;
    [SerializeField]
    Text SmallText;
    [SerializeField]
    Button Button;
    // Use this for initialization
    void Start () {
		
	}
	public void InitGameCard(string _bigText, string _smalltext)
    {
        
        BigText.text = _bigText;
        SmallText.text = _smalltext;
    }
    public void Clicked()
    {
        RenderGameDetail();
    }
    public void RenderGameDetail()
    {
        IndexController.OnGameDetail();
    }
	// Update is called once per frame
	void Update () {
		
	}
}
