﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameDetail : MonoBehaviour {
    [SerializeField]
    Text BigText;
    [SerializeField]
    Text SmallText;
    // Use this for initialization
    void Start () {
		
	}
    public void InitDetail(string _bigText, string _smalltext)
    {

        BigText.text = _bigText;
        SmallText.text = _smalltext;
    }
    public void BackClicked()
    {
        this.gameObject.active = false;
    }
    // Update is called once per frame
    void Update () {
		
	}
}
