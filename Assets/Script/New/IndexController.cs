﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IndexController : MonoBehaviour {
    //[SerializeField]
    //InputField inputSearch;
    [SerializeField]
    GameObject gameDetailObject;
    [SerializeField]
    GameObject navBarObject;
    [SerializeField]
    GameObject gameCardObject;
    [SerializeField]
    Transform gameCardParent;
    GameCard gameCard;

    public delegate void EventOnRender();
    public static EventOnRender OnGameDetail;
    public static EventOnRender OnNavBar;

    private void Awake()
    {
        OnGameDetail += ()=> {
            gameDetailObject.active = true;
        };
        OnNavBar += () => {
            navBarObject.active = true;
        };
        gameCard = gameCardObject.GetComponent<GameCard>();
    }
    private void Start()
    {
        //StartCoroutine(GetGameList());
        //gia su game list trả về game-name
        GenGameList("BattleShoot", "1.5GB - Not installed yet");

    }
    #region Button
    public void UserInfoClicked()
    {
        IndexController.OnNavBar();
    }
    #endregion
    void GenGameList(string jBigText, string jSmallText)
    {
        GameObject GameCardUnit =  Instantiate(gameCardObject, gameCardParent);
        GameCardUnit.GetComponent<GameCard>().InitGameCard( jBigText, jSmallText);
    }

    #region Async process
    IEnumerator GetGameList()
    {
        // Delete cookie before requesting a new login
        webservices.CookieString = null;

        var request = webservices.Get("own-game");
        yield return request.Send();

        if (request.isNetworkError)
        {
            Debug.LogError(request.error);
        }
        else
        {
            webservices.CookieString = request.GetResponseHeader("set-cookie");
            Debug.Log(webservices.CookieString);
            Debug.Log(request.downloadHandler.text);
            
        }
    }
    
    #endregion
}
